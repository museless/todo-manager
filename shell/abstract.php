<?php

require_once 'magento_lib.php';

/**
 * Magento Packager Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */
abstract class Mage_Shell_Base_Abstract extends Mage_Shell_Abstract {

    const ERROR_CODE_SUCCESS = 0;
    const ERROR_CODE_FAIL = 1;
    
    /**
     * prints a success message
     * @param string $message
     */
    protected function _printSuccess($message) 
    {
        $this->_printHighlighted($message, 33);
    }
    
    /**
     * prints an error message
     * @param string $message
     */
    protected function _printError($message)
    {
        $this->_printHighlighted($message, 31);
    }
    
    /**
     * prints a highlighted message
     * @param string $message
     */
    protected function _printHighlighted($message, $color=34)
    {
        echo "\t" . $this->_highlight($message, $color) . "\n";
    }
    
    /**
     * prints a highlighted message
     * @param string $message
     */
    protected function _printInformational($message, $color=36)
    {
        echo "\t" . $this->_highlight($message, $color) . "\n";
    }
    
    /**
     * prints a highlighted message
     * @param string $message
     */
    protected function _printLog($message, $color=37)
    {
        echo "\t" . $this->_highlight($message, $color) . "\n";
    }
    
    /**
     * highlights a text
     * @param type $message
     * @param type $color
     * @return string
     */
    protected function _highlight($message, $color=34)
    {
        return "\033[01;{$color}m" . $message . "\033[0m";
    }

    abstract function _run();
    
    protected function _getProcessName()
    {
        return get_class($this);
    }
    
    protected function _beforeRun()
    {
        echo "Starting " . $this->_highlight($this->_getProcessName()) . "\n";
    }
    
    protected function _afterRun()
    {
        
    }
    
    public final function run()
    {
        $this->_beforeRun();
        $result = $this->_run();
        $this->_afterRun();
        return $result;
    }

    /**
     * returns the argument or throws an error if is required an none is provided
     * @param string $name
     * @param string $default
     * @param boolean $required
     * @return string
     * @throws Exception
     */
    protected function _getArg($name, $default=null, $options=null, $messageIfRequired=null)
    {
        $value = $this->getArg($name);
        if ($value === "" || $value === false) {
            if (isset($default)) {
                $value = $default;
            } else {
                echo $this->usageHelp();
                throw new Exception("required argument \"" . (isset($messageIfRequired) ? $messageIfRequired : $name) . "\" not provided", self::ERROR_CODE_FAIL);
            }
        } elseif (isset($options)) {
            if (!in_array($value, $options)) {
                echo $this->usageHelp();
                throw new Exception("invalid argument option \"$value\" for \"$name\"", self::ERROR_CODE_FAIL);
            }
        }
        return $value;
    }
    
}