<?php

require_once 'abstract.php';

/**
 * Magento Document Source Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */
class Mage_Shell_DocumentSource extends Mage_Shell_Base_Abstract {

    /**
     * returns the process name
     * @return string
     */
    protected function _getProcessName()
    {
        return "Magento Source Documentor";
    }
    
    /**
     * returns the target directory
     * @return string
     * @throws Exception
     */
    private function _getTargetDirectory()
    {
        $value = $this->_getArg('target');
        if (!is_dir($value)) {
            throw(new Exception("target ($target) is not a directory", self::ERROR_CODE_FAIL));
        }
        return $value;
    }

    /**
     * returns the template file path
     * @return string
     * @throws Exception
     */
    private function _getTemplateFilePath()
    {
        return $this->_getArg('template');
    }

    /**
     * returns the php files to scan
     * @return string
     * @throws Exception
     */
    private function _getPhpFilesExtensions()
    {
        return $this->_getArg('php-files', 'php phtml');
    }

    /**
     * returns the xml files to scan
     * @return string
     * @throws Exception
     */
    private function _getXmlFilesExtensions()
    {
        return $this->_getArg('xml-files', 'xml');
    }
    
    /**
     * returns the verbose level
     * @return string
     */
    private function _getVerbose()
    {
        return $this->_getArg('verbose', 'full', array('full', 'summary', 'none'));
    }
    
    /**
     * returns the replace vars option
     * @return string
     */
    private function _getReplaceAntvars()
    {
        return $this->_getArg('replace-antvars', 'no', array('no', 'yes'));
    }

    /**
     * returns the replace vars option
     * @return string
     */
    private function _getAddLicense()
    {
        return $this->_getArg('add-license', 'no', array('no', 'yes'));
    }

    /**
     * returns the filenames that matchs the extensions
     * @param string $directory
     * @param string $extensions
     * @return array
     */
    private function _getFilteredFiles($directory, $extensions)
    {
        $extensions = explode(' ', preg_quote($extensions));
        $extensions = implode('|', $extensions);
        $extensions = '(' . $extensions . ')';
        $command = "find $directory -type f | egrep \"\\\\.$extensions$\"";
        $files = preg_replace("/\\s+$/m", "", `find $directory -type f | egrep "\\\\.$extensions$"`);
        if ($files === "" || !count(explode("\n", $files))) {
            return array();
        }
        $files = explode("\n", $files);

        return $files;
    }
    
    protected function _prependToFile2($file, $content) 
    {
        $handle = fopen($file, "r+");
        $len = strlen($content);
        $final_len = filesize($file) + $len;
        $cache_old = fread($handle, $len);
        rewind($handle);
        $i = 1;
        while (ftell($handle) < $final_len) {
          fwrite($handle, $content);
          $content = $cache_old;
          $cache_old = fread($handle, $len);
          fseek($handle, $i * $len);
          $i++;
        }
    }
    
    /**
     * prepends content to a file with sed
     * @param string $file
     * @param string $content
     * @param string line
     */
    protected function _prependToFile($file, $content, $line = 1)
    {
        /*
        $content = preg_quote($content);
        $content = addslashes($content);
        $content = str_replace("\n", '\n', $content);
        echo "\n" . $content . "\n\n";
         * */
        `sed -i "{$line}i $content" $file`;
    }
    
    /**
     * prepares the content to be used with sed
     * @param string $content
     * @return string
     */
    private function _prepareContent($content)
    {
        $content = preg_quote($content);
        $content = addslashes($content);
        return str_replace("\n", '\n', $content);
    }


    /**
     * documents php files
     * @param string $templateContent
     * @param string $target
     * @return array $files documented
     */
    protected function _documentPhpFiles($templateContent, $target)
    {
        $documented_files = array();
        $phpFiles = $this->_getPhpFilesExtensions();
        $files = $this->_getFilteredFiles($target, $phpFiles);
        if (!count($files)) {
            return;
        }
        
        $withPhpTags = $this->_prepareContent("<?php\n$templateContent\n?>\n");
        $templateContent = $this->_prepareContent("$templateContent\n");
        
        
        foreach ($files as $file) {
            $head = `head -n 1 $file`;
            if (!preg_match('(^\<\?php\s)', $head) || preg_match('(^\<\?php\s.*\?\>)', $head)) {
                $this->_prependToFile($file, $withPhpTags);
            } else {
                $this->_prependToFile($file, $templateContent, 2);
            }
            $documented_files[] = $file;
        }
        return $documented_files;
    }

    /**
     * documents php files
     * @param string $templateContent
     * @param string $target
     * @return array $files documented
     */
    protected function _documentXmlFiles($templateContent, $target)
    {
        $documented_files = array();
        $phpFiles = $this->_getXmlFilesExtensions();
        $files = $this->_getFilteredFiles($target, $phpFiles);
        if (!count($files)) {
            return;
        }
        
        $templateContent = $this->_prepareContent("<!--\n$templateContent\n-->\n");
        
        
        foreach ($files as $file) {
            $matchCount = preg_match('(^(?<line_number>[0-9]+)[:])', `egrep -n "[<][?]xml" $file`, $matches);
            if ($matchCount >= 1) {
                $this->_prependToFile($file, $templateContent, $matches['line_number'] + 1);
                $documented_files[] = $file;
            }
        }
        return $documented_files;
    }
    
    /**
     * replace the property tags in the content of the template
     * @param string $content
     * @return string
     */
    private function _replacePropertiesInContent($content)
    {
        $count = preg_match_all('(' . preg_quote('${antprop:') . '(?<name>[a-zA-Z0-9_-]+)' . preg_quote('}') . ')', $content, $matches);
        if ($count > 0) {
            $variables = array_unique($matches['name']);
            foreach ($variables as $variable) {
                $value = BuildProperties::get($variable);
                $content = str_replace('${antprop:' . $variable . '}', $value, $content);
            }
        }
        return $content;
    }
    
    protected function _replaceAntVars($target)
    {
        $extensions = $this->_getPhpFilesExtensions();
        $extensions .= ' ' . $this->_getXmlFilesExtensions();
        $extensions = trim($extensions);
        if ($extensions === '') {
            return;
        }
        $extensions = '-iname "*.' . preg_replace('(\\s\\b)', '" -or -iname "*.', $extensions) . '"';
        $matching = preg_quote('{antprop:') . '(?[a-zA-Z0-9_-]+)' . preg_quote('}');
        $files = `find $target -type f $extensions | xargs egrep --count "$matching" | grep -v "[:]0"`;
        $matching = "find $target -type f $extensions | xargs egrep --count \"$matching\"";
        if (!isset($files)) {
            return array();
        }
        $files = trim($files);
        $files = explode("\n", $files);
        if (!count($files)) {
            return array();
        }
        foreach ($files as $file) {
            list($file, $amount) = explode(':', $file);
            file_put_contents($file, $this->_replacePropertiesInContent(file_get_contents($file)));
        }
        return $files;
    }

    /**
     * Run script
     *
     */
    public function _run() {

        try {
            $template = $this->_getTemplateFilePath();
            $target = $this->_getTargetDirectory();
            $replaceVars = $this->_getReplaceAntvars();
            $addLicense = $this->_getAddLicense();
            $templateContent = file_get_contents($template);
            $verbose = $this->_getVerbose();
            if ($addLicense === 'yes') {
                if (empty($templateContent)) {
                    throw(new Exception("template files doesn't have any content", self::ERROR_CODE_FAIL));
                }
                $templateContent = $this->_replacePropertiesInContent($templateContent);
                $documentedFiles = array();
                $documentedFiles['php'] = $this->_documentPhpFiles($templateContent, $target);
                $documentedFiles['xml'] = $this->_documentXmlFiles($templateContent, $target);
                if (in_array($verbose, array('full', 'summary'))) {
                    foreach ($documentedFiles as $type => $files) {
                        if ($verbose === 'full') {
                            foreach ($files as $file) {
                                echo $file . "\n";
                            }
                        } else {
                            echo count($files) . ' ' . $type . " files documented\n";
                        }
                    }
                }
            }
            
            if ($replaceVars === 'yes') {
                $files = $this->_replaceAntVars($target);
                if (in_array($verbose, array('full', 'summary'))) {
                    if ($verbose === 'full') {
                        $this->_printHighlighted("Ant vars replaced in");
                        foreach ($files as $file) {
                            echo $file . "\n";
                        }
                    } else {
                        echo "Ant vars replaced in " .  count($files) . " files\n";
                    }
                }
            }


            
        } catch(Exception $e) {
            $this->_printError($e->getMessage());
            return $e->getCode();
        }

        return self::ERROR_CODE_SUCCESS;
    }
    

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp() {
        return <<<USAGE
Usage:  php -f shell/document_source.php -- [options]

  --target <version>                            [required] target directory
  --template <file>                             [required] template file of the documentation
  --php-files                                   [default=php phtml] determines which php files to document
  --xml-files                                   [default=xml] determines which xml files to document
  --verbose <level>                             [default=full] summary | none
  --replace-antvars                             [default=no] no | yes to replace ant vars in the target files
  --add-license                                 [default=yes] no | yes to add the license to the source


USAGE;
    }

}

$shell = new Mage_Shell_DocumentSource();
$errorCode = $shell->run();

//no error code
exit($errorCode);