#!/bin/bash

#delete the file
rm $2

#append file header
cat $3 >> $2

#find translations and concat to file
find $1 -type f -iname "*.php" | xargs egrep -n "(\"|')([A-Za-z]+)(\\.([A-Za-z]+))+(\"|')\\s*=" | sed "s/^/\n#/g" | sed "s/:\\s*[\"']/\\nmsgid \"/g" | sed "s/.\\s*=>\\s*./\"\\nmsgstr \"/g" | sed "s/[\"'],\?\s*$/\"/g" >> $2
