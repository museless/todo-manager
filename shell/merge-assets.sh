#!/bin/bash
pushd $1
find module -maxdepth 3 -wholename "*/public/*" | xargs -I DIR cp -Rf DIR public
popd