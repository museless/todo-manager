<?php
require_once 'abstract.php';
require_once 'Git/PatchCreator.php';

/**
 * Phpcs Check Installed
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */

$shell = new Mage_Shell_Git_PatchCreator();
$errorCode = $shell->run();

//no error code
exit($errorCode);