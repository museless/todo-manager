<?php

require_once 'abstract.php';

/**
 * Phpcs Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */
class Mage_Shell_Phpcs extends Mage_Shell_Base_Abstract {

    /**
     * returns the process name
     * @return string
     */
    protected function _getProcessName()
    {
        return "Code Sniffer";
    }
    
    /**
     * returns the target
     * @return string
     */
    private function _getTarget()
    {
        return $this->_getArg('target');
    }
    
    /**
     * return the standard file
     * @return string
     */
    private function _getStandard()
    {
        return $this->_getArg('standard', '');
    }
    
    /**
     * Run script
     *
     */
    public function _run() {

        try {
            $target = $this->_getTarget();
            $params = '';
            $standard = $this->_getStandard();
            if ($standard !== '') {
                $params .= '--standard=' . $standard . ' ';
            }

            $warningSeverity = $this->_getArg('warning-severity', '5');
            if ($standard !== '') {
                $params .= '--warning-severity=' . $warningSeverity . ' ';
            }

            $errorSeverity = $this->_getArg('error-severity', '5');
            if ($standard !== '') {
                $params .= '--error-severity=' . $errorSeverity . ' ';
            }
            
            $report_width = $this->_getArg('report-width', '80');
            if ($report_width !== '') {
                $params .= '--report-width=' . $report_width . ' ';
            }

            echo $command = "phpcs --extensions=\"php\" $target $params";
            echo $output = `$command`;
            if ($output !== null && $output !== '') {
                return self::ERROR_CODE_FAIL;
            }
            
            
        } catch(Exception $e) {
            $this->_printError($e->getMessage());
            return $e->getCode();
        }

        return self::ERROR_CODE_SUCCESS;
    }
    

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp() {
        return <<<USAGE
Usage:  php -f shell/phpcs.php -- [options]

  --target <file/dir>                           [required] target directory or file
  --standard <file>                             ruleset file for phpcs
  --warning-severity <severity>                 [default=5] severity of warnings to be hide
  --error-severity <severity>                   [default=5] severity of errors to be hide
  --report-width <width>                        [default=80] sets the width of the report

USAGE;
    }

}

$shell = new Mage_Shell_Phpcs();
$errorCode = $shell->run();

//no error code
exit($errorCode);