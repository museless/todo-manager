<?php
require_once 'abstract.php';

/**
 * Magento Document Source Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */
class Mage_Shell_FtpDeploy extends Mage_Shell_Base_Abstract {

    /**
     * returns the process name
     * @return string
     */
    protected function _getProcessName()
    {
        return "Magento Ftp Deployer";
    }
    
    private function _getTargetDir()
    {
        return $this->_getArg('target-dir', '');
    }
    
    /**
     * returns the environment name
     * @return string
     * @throws Exception
     */
    private function _getEnvironmentName()
    {
        $environement = $this->_getArg('env');
        if ($environement == 'default') {
            $environement = BuildProperties::getInstance()->getProperty('ftp.default');
        }
        return $environement;
    }
    
    /**
     * returns the path to deploy
     * @return string
     * @throws Exception
     */
    private function _getPath()
    {
        return $this->_getArg('path', '');
    }

    private function _getEnvironementVariable($ftpEnvironmentName, $variable)
    {
        $varname = $ftpEnvironmentName . "." . $variable;
        $value = BuildProperties::getInstance()->getProperty($varname);
        if (!isset($value) || !$value) {
            throw new Exception(
                "the value for \"" . $variable. "\" is not configured on $ftpEnvironmentName", 
                self::ERROR_CODE_FAIL
            );
        }
        return $value;
    }
    
    private function _getDomain($ftpEnvironmentName)
    {
        return $this->_getEnvironementVariable($ftpEnvironmentName, "domain");
    }

    private function _getUser($ftpEnvironmentName)
    {
        return $this->_getEnvironementVariable($ftpEnvironmentName, "user");
    }

    private function _getPass($ftpEnvironmentName)
    {
        return $this->_getEnvironementVariable($ftpEnvironmentName, "pass");
    }

    private function _getHome($ftpEnvironmentName)
    {
        return $this->_getEnvironementVariable($ftpEnvironmentName, "home");
    }

    /**
     * Run script
     *
     */
    public function _run() {

        try {
            $ftpEnvironmentName = $this->_getEnvironmentName();
            $ftpEnvironmentNames = explode(',', $ftpEnvironmentName);
            foreach ($ftpEnvironmentNames as $ftpEnvironmentName) {
                $domain = $this->_getDomain($ftpEnvironmentName);
                $user = $this->_getUser($ftpEnvironmentName);
                $pass = $this->_getPass($ftpEnvironmentName);
                $home = $this->_getHome($ftpEnvironmentName);
                $path = $this->_getPath();
                $targetDir = $this->_getTargetDir();

                $pass = addslashes($pass);

                if (is_dir($path)) {
                    $get = trim($path, '/') . '/*';
                } elseif (!file_exists($path)) {
                  throw new Exception("The file to deploy does not exists $path");
                } else {
                    $get = $path;
                    $path = dirname($path);
                }
                $get = preg_replace('/^src/', $targetDir, $get);
                $subpath = preg_replace('/^src/', '', trim($path, '/'));

                $user = escapeshellarg($user);
                $pass = escapeshellarg($pass);
                $target = $home . $subpath;
                //$get = escapeshellarg($get);
                $command = "ncftpput -u $user -p $pass -R $domain $target $get";
                $this->_printInformational("Uploading to $ftpEnvironmentName");
                $this->_printLog($command);
                shell_exec($command);
            }
            
        } catch(Exception $e) {
            $this->_printError($e->getMessage());
            return $e->getCode();
        }

        return self::ERROR_CODE_SUCCESS;
    }
    

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp() {
        return <<<USAGE
Usage:  php -f shell/ftp_deploy.php -- [options]

  --env <name>                              [required] ftp environment name
  --path    <path>                              [required] path to deploy
  --target-dir <path>                           [required]


USAGE;
    }

}

$shell = new Mage_Shell_FtpDeploy();
$errorCode = $shell->run();

exit($errorCode);
