<?php
require_once 'abstract.php';

/**
 * Magento Remove Files Shell Script
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */
class Mage_Shell_RemoveFiles extends Mage_Shell_Base_Abstract {

    /**
     * returns the process name
     * @return string
     */
    protected function _getProcessName()
    {
        return "Remove Files";
    }
    
    /**
     * Run script
     *
     */
    public function _run() {

        try {
            $target = $this->_getArg('target');
            $loadGlobFile = $this->_getArg('load-glob-file');
            if (!is_dir($target)) {
                throw new Exception("target ($target) must be a directory. ");
            }
            if (!is_file($loadGlobFile)) {
                $this->_printInformational("[skipping] load-glob-file ($loadGlobFile) must be a file containing glob entries.");
            } else {
            
                $content = explode("\n", trim(file_get_contents($loadGlobFile)));
                if (!count($content) || $content[0] === "") {
                   $this->_printHighlighted("File ($loadGlobFile) is empty");
                } else {
                    foreach ($content as $line) {
                        $files = `find $target -wholename "*$line"`;
                        $files = trim($files);
                        $files = explode("\n", $files);
                        $count = count($files);
                        if ($files == array("")) {
                            $count = 0;
                        }
                        $this->_printInformational("Matching $line in $target ($count)");
                        if ($files != "" && $count != 0) {
                            echo "\t[deleting] " . implode("\n\t[deleting] ", $files) . "\n";
                            foreach ($files as $file) {
                                unlink($file);
                            }
                        }
                    }
                }
            }
            
        } catch(Exception $e) {
            $this->_printError($e->getMessage());
            return $e->getCode();
        }

        return self::ERROR_CODE_SUCCESS;
    }
    

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp() {
        return <<<USAGE
Usage:  php -f shell/remove_files.php -- [options]

  --target <directory>                          [required] target destination
  --load-glob-file <file>                       [required] file contining glob exclusions

USAGE;
    }

}

$shell = new Mage_Shell_RemoveFiles();
$errorCode = $shell->run();


