<?php

error_reporting(-1);
ini_set('display_errors', '1');


/**
 * Ant build properties reader
 *
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */
class BuildProperties
{
    /**
     *
     * @var BuildProperties
     */
    private static $_instance;
    
    /**
     *
     * @var array
     */
    private $_properties;
    
    private static $_cwd = null;
    
    /**
     * Class constructor
     */
    public function __construct()
    {
        self::$_cwd = getcwd();
        $this->_properties = parse_ini_file(self::$_cwd . '/build.properties');
    }
    
    public static function getProjectPath()
    {
        return self::$_cwd . '/';
    }
    
    /**
     * returns build properties
     * @return BuildProperties
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    /**
     * reads a property
     * @param string $name
     * @return mixed
     */
    public function getProperty($name)
    {
        if (isset($this->_properties[$name])) {
            return $this->_properties[$name];
        }
        return null;
    }
    
    /**
     * reads a property statically
     * @param string $name
     * @return mixed
     */
    public static function get($name)
    {
        return self::getInstance()->getProperty($name);
    }
}
/**
 * add the shell abstract library
 */
//require_once BuildProperties::get('magento.path') . 'shell/abstract.php';
require_once "mage_shell.php";

