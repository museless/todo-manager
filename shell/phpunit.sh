#!/bin/bash

result=0
for f in `find src/module -type d -iname "test"`
do
 echo "PHPUnit on $f"
 pushd $f
 thisok=1
 phpunit && thisok=0
 if [[ "$thisok" == "1" ]]; then
   result=1
 fi
 popd
done

exit $result
