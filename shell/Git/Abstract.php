<?php
/**
 * Description of Abstract
 *
 * @author root
 */
require_once 'Model/LogElement.php';

abstract class Mage_Shell_Git_Abstract extends Mage_Shell_Base_Abstract {
    
    /**
     * 
     * @param array $log contents
     * @param int $pos position from where to start looking
     * @param int $cant amount of array elements used
     * @return array
     */
    private function _getLogElement(array $log, int $pos, int &$cant = null)
    {
        $cant = 0;
        $logElement = array();
        for ($i = $pos; $i < count($log); $i++) {
            if (preg_match('(^commit (?P<version>[a-zA-Z0-9]+))', $log[$i], $matches)) {
                if (isset($logElement['version'])) {
                    break;
                } else {
                    $logElement['version'] = $matches['version'];
                }
            } elseif(!isset($logElement['version'])) {
                return array();
            } elseif(preg_match('(^(?P<variable>[^:]+)[:](?P<content>.*))', $log[$i], $matches)) {
                $logElement[$matches['variable']] = trim($matches['content']);
            } else {
                if (trim($log[$i]) !== '') {
                    $logElement['description'] .= (count($logElement['description']) ? "\n" : "") . trim($log[$i]);
                }
            }
            $cant ++;
        }
        return $logElement;
    }
    
    /**
     * 
     * @return array Git_Model_LogElement
     */
    protected function _getLog()
    {
        $log = `git log`;
        $log = explode("\n", $log);
        $logElements = array();
        for ($i = 0; $i < count($log); $i++) {
            $logElement = $this->_getLogElement($log, $i, $cant);
            $i += $cant - 1;
            if (count($logElement)) {
                $logElement['secuential'] = count($logElements);
                $logElement = new Git_Model_LogElement($logElement);
                $logElements[] = $logElement;
            }
        }
        return $logElements;
        //var_dump($log);
    }
    
    /**
     * returns the content of the file version
     * @param string $file
     * @param Git_Model_LogElement $version
     * @return string
     */
    protected function _getFileVersion(string $file, Git_Model_LogElement $version)
    {
        if ($version->getVersion() !== null) {
            return `git show {$version}:$file`;
        }
        return null;
    }
}
?>
