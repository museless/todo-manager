<?php
require_once 'Abstract.php';

/**
 * Phpcs Check Installed
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */
class Mage_Shell_Git_PatchCreator extends Mage_Shell_Git_Abstract {
    const BUILD_FILE = 'build.properties';
    /**
     * returns the process name
     * @return string
     */
    protected function _getProcessName()
    {
        return "Patch creator";
    }
    
    /**
     * returns the versions
     * @return array Git_Model_LogElement
     */
    protected function _getVersions()
    {
        $logs = $this->_getLog();
        return array_reverse($logs, true);
    }
    
    protected function _listPacakgeVersions()
    {
        echo "listing versions\n";
        $logs = $this->_getVersions();
        foreach ($logs as $i=>$log) {
            $properties = parse_ini_string($this->_getFileVersion(self::BUILD_FILE, $log));
            echo 'Version: ' . $properties['version'] . '/' . $log->getSecuential() . "\n";
            echo 'Description: ' . $log->getDescription() . "\n";
            echo "\n";
        }
    }
    
    protected function _getVersion(string $version)
    {
        $composed = false;
        if (strpos($version, '/') !== false) {
            $composed = true;
        }
        $version = explode('/', $version);
        
        $logs = $this->_getLog();
        foreach ($logs as $i=>$log) {
            $properties = parse_ini_string($this->_getFileVersion(self::BUILD_FILE, $log));
            if ($composed === true && $version[1] == $log->getSecuential() && $version[0] === $properties['version']) {
                return $log;
            } elseif ($composed === false && $version[0] === $properties['version']) {
                return $log;
            }
        }
        return null;
    }
    
    protected function _createPatch(string $from, string $to, string $tmpDir, string $targetDir)
    {
        $fromSanitized = str_replace('/', '-', $from);
        $toSanitized = str_replace('/', '-', $to);
        $versionFrom = $this->_getVersion($from);
        $versionTo = $this->_getVersion($to);
        
        if (!isset($versionFrom)) {
            throw new Exception("wrong version 'from' {$from}", self::ERROR_CODE_FAIL);
        }
        if (!isset($versionTo)) {
            throw new Exception("wrong version 'to' {$to}", self::ERROR_CODE_FAIL);
        }
        
        echo "creating patch from $from to $to\n";
        echo "from: " . $versionFrom->getVersion() . " - " . $versionFrom->getDescription() . "\n";
        echo "to: " . $versionTo->getVersion() . " - " . $versionTo->getDescription() . "\n";
        
        `mkdir -p $tmpDir`;
        `mkdir -p $targetDir`;
        
        $targetDiffName = $targetDir . '/' . 'git_diff_' . $fromSanitized . '_to_' . $toSanitized . '.diff';
        
        $commitFrom = $versionFrom->getCommit();
        $commitTo = $versionTo->getCommit();
        `git diff --relative=src $commitFrom $commitTo > $targetDiffName`;

        if (!file_exists($targetDiffName)) {
            throw new Exception("Couldn't create diff $targetDiffName", self::ERROR_CODE_FAIL);
        }
        echo "Diff created: ";
        $this->_printSuccess("$targetDiffName");
        
        return;

        
        $targetDirFrom = $targetDir . '/' . 'version-' . $fromSanitized;
        if (file_exists($targetDirFrom)) {
            `rm -Rf $targetDirFrom`;
        }
        `mkdir -p $targetDirFrom`;
        
        $targetDirTo = $targetDir . '/' . 'version-' . $toSanitized;
        if (file_exists($targetDirTo)) {
            `rm -Rf $targetDirTo`;
        }
        `mkdir -p $targetDirTo`;
        
        $cwd = getcwd();
        `git clone --local . $targetDirFrom`;
        `git clone --local . $targetDirTo`;
        
        chdir($targetDirFrom);
        $version = $versionFrom->getVersion();
        `git checkout -q $version`;
        chdir($targetDirTo);
        $version = $versionTo->getVersion();
        `git checkout -q $version`;
        chdir($cwd);
        
        
        `git diff -- $targetDirFrom/src $targetDirTo/src > $targetDiffName`;
        if (!file_exists($targetDiffName)) {
            throw new Exception("Couldn't create diff $targetDiffName", self::ERROR_CODE_FAIL);
        }
        echo "Diff created: ";
        $this->_printSuccess("$targetDiffName");
    }

    /**
     * Run script
     *
     */
    public function _run() {

        $cwd = null;
        
        try {
            
            $list = $this->_getArg('list', 'no');
            if ($list === 'yes') {
                $this->_listPacakgeVersions();
                return self::ERROR_CODE_SUCCESS;
            }
            $from = $this->_getArg('from');
            $to = $this->_getArg('to');
            $tmp = $this->_getArg('tmp');
            $target = $this->_getArg('target');
            
            $installed = false;
            if ($from === '') {
                throw new Exception('--from parameter required', self::ERROR_CODE_FAIL);
            }
            if ($to === '') {
                throw new Exception('--to parameter required', self::ERROR_CODE_FAIL);
            }
            
            $this->_createPatch($from, $to, $tmp, $target);
            /*
            if (($errorOn === 'installed' && $installed === true) ||
                   ($errorOn === 'notinstalled' && $installed === false)) {
                return self::ERROR_CODE_FAIL;
            }*/
            
            
        } catch(Exception $e) {
            chdir($cwd);
            $this->_printError($e->getMessage());
            return $e->getCode();
        }

        return self::ERROR_CODE_SUCCESS;
    }
    

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp() {
        return <<<USAGE
Usage:  php -f shell/createpatch.php -- [options]

  --list <option>                               [default=no] yes | no, if you need to list the versions
  --from <version>                              [required] magento extension version
  --to <version>                                [required] magento extension version
  --tmp <tempdir>                               [required] temporary directory to use
  --target <directory>                          [required] directory to place the patch

USAGE;
    }
}

?>
