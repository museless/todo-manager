<?php

/**
 * Description of LogElement
 *
 * @author root
 */
class Git_Model_LogElement {
    
    /**
     *
     * @var array
     */
    private $_data = array();
    
    public function __construct($data)
    {
        $this->_data = $data;
    }
    
    private function _getData($key)
    {
        return isset($this->_data[$key]) ? $this->_data[$key] : null;
    }
    
    public function getVersion()
    {
        return $this->_getData('version');
    }

    public function getAuthor()
    {
        return $this->_getData('Author');
    }

    public function getDate()
    {
        return $this->_getData('Date');
    }

    public function getDescription()
    {
        return $this->_getData('description');
    }

    public function getSecuential()
    {
        return $this->_getData('secuential');
    }
    
    public function getVersionComposed()
    {
        return $this->getPackageVersion() . '/' . $this->getSecuential();
    }
    
    public function getCommit()
    {
        return $this->getVersion();
    }
}

?>
