<?php
require_once 'abstract.php';

/**
 * Preserves the deploy local files before redeploy
 *
 * @category    Mage
 * @package     Mage_Shell
 * @author      Hugo Mauricio Prado Macat <mauricioprado00@gmail.com>
 */
class Mage_Shell_DeployPreserve extends Mage_Shell_Base_Abstract {

    /**
     * returns the process name
     * @return string
     */
    protected function _getProcessName()
    {
        return "Preserve deploy local files";
    }
    
    /**
     * Run script
     *
     */
    public function _run() {

        try {
            $targetDir = $this->_getArg('target-dir');
            $sourceDir = $this->_getArg('source-dir');
            $loadGlobFile = $this->_getArg('load-glob-file');

            if (!is_dir($targetDir)) {
                throw new Exception("target-dir ($targetDir) must be a directory. ");
            }
            if (!is_dir($sourceDir)) {
                throw new Exception("source-dir ($sourceDir) must be a directory. ");
            }
            if (!is_file($loadGlobFile)) {
                $this->_printInformational("[skipping] load-glob-file ($loadGlobFile) must be a file containing glob entries.");
            } else {
                $content = explode("\n", trim(file_get_contents($loadGlobFile)));
                if (!count($content) || $content[0] === "") {
                   $this->_printHighlighted("File ($loadGlobFile) is empty");
                } else {
                    foreach ($content as $line) {
                        $files = `find $targetDir -wholename "*$line"`;
                        $files = trim($files);
                        $files = explode("\n", $files);
                        $count = count($files);
                        if ($files == array("")) {
                            $count = 0;
                        }
                        $this->_printInformational("Matching $line in $targetDir ($count)");
                        if ($files != "" && $count != 0) {
                            echo "\t[preserving] " . implode("\n\t[preserving] ", $files) . "\n";
                            foreach ($files as $file) {
                                $path = str_replace($targetDir, "", $file);
                                $sourcePath = $sourceDir . $path;
                                $sourcePathDir = $sourceDir . dirname($path);
                                if (!is_dir($sourcePathDir)) {
                                    mkdir($sourcePathDir, 0777, true);
                                }
                                copy($file, $sourcePath);
                            }
                        }
                    }
                }
            }
            
        } catch(Exception $e) {
            $this->_printError($e->getMessage());
            //return $e->getCode();
            return 1;
        }

        return self::ERROR_CODE_SUCCESS;
    }
    

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp() {
        return <<<USAGE
Usage:  php -f shell/deploy_preserve.php -- [options]

  --source-dir <directory>                      [required] source directory
  --target-dir <directory>                      [required] target directory
  --load-glob-file <file>                       [required] file contining glob exclusions

USAGE;
    }

}

$shell = new Mage_Shell_DeployPreserve();
$errorCode = $shell->run();

exit($errorCode);



