<?php
namespace TodoTest\Controller;

use TodoTest\Bootstrap;
use Todo\Controller\TodoController;
use Todo\Model\TodoTable;
use Todo\Model\Todo;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class TodoControllerTest extends PHPUnit_Framework_TestCase
{
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        
        $mockTableGateway = $this->getMock(
            'Zend\Db\TableGateway\TableGateway',
            array('select'), 
            array(), 
            '', 
            false
        );
        
        /**
         * mock the select results on table gateway
         */
        $todoData = array(
            'id' => 123, 
            'description' => 'some description', 
            'completed' => 'some completed',
            'duedate' => 'some duedate',
            'priority' => 'some priority',
        );
        $todo     = new Todo();
        $todo->exchangeArray($todoData);

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Todo());
        $resultSet->initialize(array($todo));
        
        $mockTableGateway->expects($this->any())
            ->method('select')
            ->will($this->returnValue($resultSet));
        $todoTable = new TodoTable($mockTableGateway);

        $this->controller = new TodoController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'index'));
        $this->event      = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);
        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
        $this->controller->setTodoTable($todoTable);
        $this->markTestSkipped('Deprecated class TodoController');
    }

    public function testAddActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'add');

        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDeleteActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'delete');
        $this->routeMatch->setParam('id', '1');

        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDeleteActionRedirect()
    {
        $this->routeMatch->setParam('action', 'delete');

        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(302, $response->getStatusCode());
    }
    
    public function testEditActionRedirect()
    {
        $this->routeMatch->setParam('action', 'edit');

        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(302, $response->getStatusCode());
    }

    public function testEditActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'edit');
        $this->routeMatch->setParam('id', '1');

        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'index');
        
        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testGetTodoTableReturnsAnInstanceOfTodoTable()
    {
        $this->assertInstanceOf('Todo\Model\TodoTable', $this->controller->getTodoTable());
    }
}
