<?php

namespace TodoTest\Controller;

use TodoTest\Bootstrap;
use Todo\Controller\TodoRestApiController;
use Todo\Controller\Plugin\ValidateApiToken;
use Todo\Model\TodoTable;
use Todo\Model\Todo;
use Todo\Form\TodoForm;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class TodoRestApiControllerTest extends PHPUnit_Framework_TestCase
{

    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    protected function setUp()
    {
        
        $mockTableGateway = $this->getMock(
            'Zend\Db\TableGateway\TableGateway',
            array('select','insert','delete','update'), 
            array(), 
            '', 
            false
        );
        
        /**
         * mock the select results on table gateway
         */
        $todoData = array(
            'id' => 123, 
            'description' => 'some description', 
            'completed' => '1',
            'duedate' => '2013-10-26T23:59',
            'priority' => '5',
        );
        $todo = new Todo();
        $todo->exchangeArray($todoData);

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Todo());
        $resultSet->initialize(array($todo));
        
        $mockTableGateway->expects($this->any())
            ->method('select')
            ->will($this->returnValue($resultSet));
        $todoTable = new TodoTable($mockTableGateway);
        
        /**
         * mock the token validator
         */
        $validateApiToken = $this->getMock(
            'Todo\Controller\Plugin\ValidateApiToken',
            array('validate'), array(), '', false
        );
        
        $validateApiToken->expects($this->any())
            ->method('validate')
            ->will($this->returnValue(true));
        
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new TodoRestApiController();
        $this->request = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'index'));
        $this->event = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);
        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->getPluginManager()
           ->setService('validateApiToken', $validateApiToken);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
        $this->controller->setTodoTable($todoTable);
    }

    public function testGetListCanBeAccessed()
    {
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetCanBeAccessed()
    {
        $this->routeMatch->setParam('id', '1');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @dataProvider rightDataProvider
     */
    public function testCreateCanBeAccessed($rightData)
    {
        $this->request->setMethod('post');
        foreach ($rightData as $key=>$value) {
            $this->request->getPost()->set($key, $value);
        }
        
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @dataProvider wrongDataProvider
     */
    public function testCreateWillThrowErrorIfWrongData($wrongData)
    {
        $this->request->setMethod('post');
        foreach ($wrongData as $key=>$value) {
            $this->request->getPost()->set($key, $value);
        }
        
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(412, $response->getStatusCode());
    }


    /**
     * @dataProvider wrongDataProvider
     */
    public function testUpdateWillThrowErrorIfWrongData($wrongData)
    {
        $this->routeMatch->setParam('id', '1');
        $this->request->setMethod('put');
        $this->request->setContent(http_build_query($wrongData));
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(412, $response->getStatusCode());
    }
    
    /**
     * @dataProvider rightDataProvider
     */
    public function testUpdateCanBeAccessed($rightData)
    {
        $this->routeMatch->setParam('id', '1');
        $this->request->setMethod('put');
        $this->request->setContent(http_build_query($rightData));

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function wrongDataProvider()
    {
        return array(
            array(
                array(
                    'description' => '',//missing description
                    'completed' => '1',
                    'duedate' => '2013-10-26T23:59',
                    'priority' => '5',
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => '',//missing completed
                    'duedate' => '2013-10-26T23:59',
                    'priority' => '5',
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => 'true',//wrong completed value
                    'duedate' => '2013-10-26T23:59',
                    'priority' => '5',
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => '1',
                    'duedate' => '',//missing due date
                    'priority' => '5',
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => '1',
                    'duedate' => '2013-05-05 13:30',//wrong date format
                    'priority' => '5',
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => '1',
                    'duedate' => '2013-10-26T23:59',
                    'priority' => '',//missing priority
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => '1',
                    'duedate' => '2013-10-26T23:59',
                    'priority' => '11',//wrong priority
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => '1',
                    'duedate' => '2013-10-26T23:59',
                    'priority' => '-1',//wrong priority
                ),
            ),
        );
    }

    public function rightDataProvider()
    {
        return array(
            array(
                array(
                    'description' => 'some description',
                    'completed' => '1',
                    'duedate' => '2013-10-26T23:59',
                    'priority' => '5',
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => '1',
                    'duedate' => '2013-10-26 23:59:00',//allowed alternative data format
                    'priority' => '5',
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => '1',//one allowed
                    'duedate' => '2013-10-26 23:59:00',//allowed alternative data format
                    'priority' => '1',//minimal value
                ),
            ),
            array(
                array(
                    'description' => 'some description',
                    'completed' => '0',//zero allowed
                    'duedate' => '2013-10-26 23:59:00',//allowed alternative data format
                    'priority' => '10',//max value
                ),
            ),
        );
    }

    public function testDeleteCanBeAccessed()
    {
        $this->routeMatch->setParam('id', '1');
        $this->request->setMethod('delete');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testGetTodoTableReturnsAnInstanceOfTodoTable()
    {
        $this->assertInstanceOf('Todo\Model\TodoTable', $this->controller->getTodoTable());
    }

}