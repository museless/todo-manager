<?php

namespace TodoTest\Model;

use Todo\Model\Todo;
use PHPUnit_Framework_TestCase;

class TodoTest extends PHPUnit_Framework_TestCase
{
    public function testTodoInitialState()
    {
        $todo = new Todo();

        $this->assertNull($todo->id, '"id" should initially be null');
        $this->assertNull($todo->description, '"description" should initially be null');
        $this->assertNull($todo->completed, '"completed" should initially be null');
        $this->assertNull($todo->duedate, '"duedate" should initially be null');
        $this->assertNull($todo->priority, '"priority" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $todo = new Todo();
        $data  = array(
            'id'     => 123,
            'description' => 'some description',
            'completed'  => 'some completed',
            'duedate' => 'some duedate', 
            'priority' => 'some priority',
        );

        $todo->exchangeArray($data);

        $this->assertSame($data['id'], $todo->id, '"id" was not set correctly');
        $this->assertSame($data['description'], $todo->description, '"description" was not set correctly');
        $this->assertSame($data['completed'], $todo->completed, '"completed" was not set correctly');
        $this->assertSame($data['duedate'], $todo->duedate, '"duedate" was not set correctly');
        $this->assertSame($data['priority'], $todo->priority, '"priority" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $todo = new Todo();

        $todo->exchangeArray(
            array(
                'id'     => 123,
                'description' => 'some description',
                'completed'  => 'some completed',
                'duedate'  => 'some duedate',
                'priority'  => 'some priority',
            )
        );
        $todo->exchangeArray(array());

        $this->assertNull($todo->id, '"id" should have defaulted to null');
        $this->assertNull($todo->description, '"description" should have defaulted to null');
        $this->assertNull($todo->completed, '"completed" should have defaulted to null');
        $this->assertNull($todo->duedate, '"duedate" should have defaulted to null');
        $this->assertNull($todo->priority, '"priority" should have defaulted to null');
    }
}