<?php

namespace TodoTest\Model;

use Todo\Model\TodoTable;
use Todo\Model\Todo;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class TodoTableTest extends PHPUnit_Framework_TestCase
{
    public function testFetchAllReturnsAllTodos()
    {
        $resultSet        = new ResultSet();
        $mockTableGateway = $this->getMock(
            'Zend\Db\TableGateway\TableGateway',
            array('select'), array(), '', false
        );

        $mockTableGateway->expects($this->once())
                         ->method('select')
                         ->with(array('user_id' => 1))
                         ->will($this->returnValue($resultSet));

        $todoTable = new TodoTable($mockTableGateway);

        $this->assertSame($resultSet, $todoTable->fetchAll(1));
    }
    
    public function testCanRetrieveAnTodoByItsId()
    {
        $todo = new Todo();
        $todo->exchangeArray(
            array(
                'id'     => 123,
                'description' => 'some description', 
                'completed' => 'some completed',
                'duedate' => 'some duedate', 
                'priority' => 'some priority',
            )
        );

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Todo());
        $resultSet->initialize(array($todo));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                         ->method('select')
                         ->with(array('id' => 123, 'user_id' => 1))
                         ->will($this->returnValue($resultSet));

        $todoTable = new TodoTable($mockTableGateway);

        $this->assertSame($todo, $todoTable->getTodo(123, 1));
    }

    public function testCanDeleteAnTodoByItsId()
    {
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('delete'), array(), '', false);
        $mockTableGateway->expects($this->once())
                         ->method('delete')
                         ->with(array('id' => 123, 'user_id' => 1));

        $todoTable = new TodoTable($mockTableGateway);
        $todoTable->deleteTodo(123, 1);
    }

    public function testSaveTodoWillInsertNewTodosIfTheyDontAlreadyHaveAnId()
    {
        $todoData = array(
            'description' => 'some description', 
            'completed' => 'some completed',
            'duedate' => 'some duedate', 
            'priority' => 'some priority',
        );
        $newId = 99;
        $todo     = new Todo();
        $todo->exchangeArray($todoData);

        $mockTableGateway = $this->getMock(
            'Zend\Db\TableGateway\TableGateway', 
            array('insert', 'getLastInsertValue'), array(), '', false
        );
        $mockTableGateway->expects($this->once())
                         ->method('insert')
                         ->with(array_merge($todoData, array('user_id'=>1)));
        $mockTableGateway->expects($this->once())
                         ->method('getLastInsertValue')
                         ->will($this->returnValue($newId));

        $todoTable = new TodoTable($mockTableGateway);
        $id = $todoTable->saveTodo($todo, 1);
        $this->assertEquals($id, $newId, "saveTodo should return new id when the object does not have one");
    }

    public function testSaveTodoWillUpdateExistingTodosIfTheyAlreadyHaveAnId()
    {
        $todoData = array(
            'id' => 123,
            'description' => 'some description', 
            'completed' => 'some completed',
            'duedate' => 'some duedate', 
            'priority' => 'some priority',
        );
        $todo     = new Todo();
        $todo->exchangeArray($todoData);

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Todo());
        $resultSet->initialize(array($todo));

        $mockTableGateway = $this->getMock(
            'Zend\Db\TableGateway\TableGateway',
            array('select', 'update'), array(), '', false
        );
        $mockTableGateway->expects($this->once())
                         ->method('select')
                         ->with(array('id' => 123, 'user_id' => 1))
                         ->will($this->returnValue($resultSet));
        $mockTableGateway->expects($this->once())
            ->method('update')
            ->with(
                array(
                    'description' => 'some description', 
                    'completed' => 'some completed',
                    'duedate' => 'some duedate', 
                    'priority' => 'some priority',
                ),
                array('id' => 123, 'user_id' => 1)
            );

        $todoTable = new TodoTable($mockTableGateway);
        $id = $todoTable->saveTodo($todo, 1);
        $this->assertEquals($id, $todoData['id'], "saveTodo should return the updated id todo");
    }

    public function testExceptionIsThrownWhenGettingNonexistentTodo()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Todo());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                         ->method('select')
                         ->with(array('id' => 123, 'user_id' => 1))
                         ->will($this->returnValue($resultSet));

        $todoTable = new TodoTable($mockTableGateway);

        try
        {
            $todoTable->getTodo(123, 1);
        }
        catch (\Exception $e)
        {
            $this->assertSame('Could not find row 123', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }
}