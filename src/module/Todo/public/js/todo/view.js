//View

/*global window */
/*global Backbone*/
/*global $*/
/*global _*/
/*global TodoListItemView*/
/*global TodoListViewHeaders*/
/*jslint unparam: true*/
/*global t*/

var global = window || {};

$(function () {
    "use strict";

    global.TodoListView = Backbone.View.extend({
        tagName: 'div',

        template: null,

        /**
         * @type {Object}
         */
        viewTemplates: null,

        sortFields: {},

        _sortTransitions: {
            none: 'asc',
            asc: 'desc',
            desc: 'none'
        },

        initialize: function (options) {
            this.fieldsLabels = options.fieldsLabels || {};
            this.model.bind("reset", this.render, this);
            this.model.bind("add", this._addTodo.bind(this));
            this.viewTemplates = options.viewTemplates || {};
            this.template = _.template(
                $(this.viewTemplates.templateTodoList || '#tpl-todo-list').html()
            );
        },

        events: {
            "click .new": "newTodo"
        },

        newTodo: function (event) {
            this.trigger("newTodo", {view: this});
            return false;
        },

        _renderTodo: function (todo) {
            return new TodoListItemView({model: todo, viewTemplates: this.viewTemplates}).render().el;
        },

        _addTodo: function (todo) {
            $(this.el).find('.todo-list-items').append(this._renderTodo(todo));
        },

        _renderFixedHeader: function () {
            var $self = $(this.el),
                $header = $self.find('table:first thead').clone(),
                bindedOnScroll = this._onWindowScroll.bind(this, $(this.el).offset().top);
            //unbind the scroll, clean the fixedHeader
            this.unbindOnScroll();
            this.fixedHeader = $self.find('.header-fixed').html($header);
            this._recalculateFixedHeaderWidths();
            $(window).bind("scroll", bindedOnScroll);
            this.unbindOnScroll = this._unbindOnScroll.bind(this, bindedOnScroll);
            this._bindSorting($header);
        },

        _recalculateFixedHeaderWidths: function () {
            var $self = $(this.el);
            this.fixedHeader.find('th').each(function (i, el) {
                var width = $self.find('table thead th:eq(' + i + ')').width();
                $(el).width(width);
            });
        },

        unbindOnScroll: function () {
            return null;
        },

        _unbindOnScroll: function (bindedOnScroll) {
            this.fixedHeader = null;
            $(window).unbind("scroll", bindedOnScroll);
        },

        _onWindowScroll: function (tableOffset) {
            var offset = $(window).scrollTop() + 50;

            if (offset >= tableOffset && this.fixedHeader.is(":hidden")) {
                this.fixedHeader.show();
            } else if (offset < tableOffset) {
                this.fixedHeader.hide();
            }
        },

        render: function (data) {
            window.setTimeout(this._renderFixedHeader.bind(this));
            $(window).bind('resize', this._recalculateFixedHeaderWidths.bind(this));
            if (this.model.models.length > 0) {
                //render from template
                $(this.el).html(this.template());
                //add the headers
                $(this.el).find('.todo-list-head-items').append(
                    new TodoListViewHeaders({model: this.fieldsLabels, viewTemplates: this.viewTemplates}).render().el
                );
                //add the items
                _.each(this.model.models, this._addTodo.bind(this));
                //bind sorting elements
                this._bindSorting($(this.el));
                return this;
            }
            return this.renderEmpty(data);
        },

        _bindSorting: function ($els) {
            $els.find('[data-sortby]').each(function (idx, el) {
                var sortField = $(el).data().sortby;
                $(el).bind('click', this._sortByField.bind(this, sortField, el));
                if (this.sortFields[sortField] === undefined) {
                    this.sortFields[sortField] = 'none';
                } else {
                    $(el).addClass('sorted-' + this.sortFields[sortField]);
                }
            }.bind(this));
        },

        _sortByField: function (sortField, el) {
            var currentSort = this.sortFields[sortField],
                newSort = this._sortTransitions[currentSort];

            $(this.el).find('[data-sortby=' + sortField + ']')
                .removeClass('sorted-' + currentSort)
                .addClass('sorted-' + newSort);
            this.sortFields[sortField] = newSort;
            this.model.setMultiFieldComparator(this.sortFields);
            if (this.model.comparator) {
                this.model.sort();
                this._rerenderSorted(false);
            }
        },

        _rerenderSorted: function (hard) {
            var currentItems = $(this.el).find('.todo-list-items > tr');
            _.each(this.model.models, this._addTodo.bind(this));
            currentItems.remove();
        },

        relocateTodo: function (todo) {
            if (!this.model.comparator) {
                return;
            }
            var id = todo.get('id'),
                item = $(this.el).find('.todo-item-id' + id),
                after = null;
            this.model.sort();
            this.model.models.every(function (another) {
                if (another.get('id') === todo.get('id')) {
                    return;
                }
                after = another;
                return true;
            });

            if (after === null) {
                $(this.el).find('.todo-list-items').prepend(item);
            } else {
                $(this.el).find('.todo-item-id' + after.id).after(item);
            }
        },

        revealTodo: function (todo) {
            var id = todo.get('id'),
                item = $(this.el).find('.todo-item-id' + id),
                top = 0;
            if (id && item.length) {
                top -= $(this.el).find('.header-fixed').height();
                $('html,body').animate({
                    scrollTop: item.offset().top + top
                }, 1000);
            }
        },

        renderEmpty: function () {
            $(this.el).html(_.template($(this.viewTemplates.templteTodoListEmpty).html())());
            return this;
        },

        startEditing: function (id) {
            var item = $(this.el).find('.todo-item-id' + id);
            item.addClass('editing');
            $(this.el).addClass('editing');
            $('html, body').animate({
                scrollTop: item.offset().top - 50
            }, 1000);
        },

        getEditingElementHeight: function () {
            return $(this.el).find('.editing').height();
        },

        getEditingElementWidth: function () {
            return $(this.el).find('.editing').width();
        },

        getWidth: function () {
            return $(this.el).width();
        },

        endEditing: function () {
            $(this.el).removeClass('editing');
            $(this.el).find('.editing').removeClass('editing');
        }
    });

    global.TodoListViewHeaders = Backbone.View.extend({

        tagName: "tr",

        template: _.template($('#tpl-todo-list-head-item').html()),

        /**
         * @type {Object}
         */
        viewTemplates: null,

        initialize: function (options) {
            this.viewTemplates = options.viewTemplates || {};
            this.template = _.template(
                $(this.viewTemplates.templateTodoListHeadItem || '#tpl-todo-list-head-item').html()
            );
        },

        render: function () {
            $(this.el).html(this.template(this.model));
            return this;
        }
    });

    global.TodoListItemView = Backbone.View.extend({

        tagName: "tr",

        template: null,

        /**
         * @type {Object}
         */
        viewTemplates: null,

        initialize: function (options) {
            this.model.bind("sync", this.render, this);
            this.model.bind("destroy", this.close, this);
            this.viewTemplates = options.viewTemplates || {};
            this.template = _.template(
                $(this.viewTemplates.templateTodoListItem || '#tpl-todo-list-item').html()
            );
        },

        render: function (eventName) {
            var classes = "todo-item-id" + this.model.get('id') + " todo-item todo-item";
            classes += (this.model.get('completed') === '1' ? '-completed' : '-uncompleted');
            classes += "";
            $(this.el)
                .html(this.template(this.model.getReadableLocalizedJSON()))
                .attr("class", classes);
            return this;
        },

        close: function () {
            $(this.el).unbind();
            $(this.el).remove();
        }
    });

    global.TodoView = Backbone.View.extend({

        template: null,

        /**
         * @type {Object}
         */
        viewTemplates: null,

        originalAttributes: null,

        initialize: function (options) {
            this.viewTemplates = options.viewTemplates || {};
            this.template = _.template(
                $(this.viewTemplates.templateTodoDetails || '#tpl-todo-details').html()
            );
        },

        render: function (renderData) {
            var todo = this.model.clone()
                    .set({duedate: this.model.getDatetimeLocal()});
            this.originalAttributes = todo.clone().attributes;
            $(this.el)
                .attr('class', 'todo-edit-view')
                .html(this.template(_.extend({error: ""}, todo.toJSON(), renderData)));
            if (!this.model.isNew() && this.model.get('id')) {
                this.trigger('startEditing', this.model.get('id'));
            }
            return this;
        },

        events: {
            "change input": "change",
            "click .save": "saveTodo",
            "click .delete": "deleteTodo",
            "click .cancel": "cancel"
        },

        change: function (event) {
            var target = event.target;
            window.console.log('changing ' + target.id + ' from: ' + target.defaultValue + ' to: ' + target.value);
            window.console.log("add validations");
        },

        getDataFromUI: function () {
            return {
                description: $('#description').val(),
                completed: $('#completed').is(':checked') ? 1 : 0,
                duedate: $('#duedate').val(),
                priority: $('#priority').val()
            };
        },

        saveTodo: function () {
            this.model.set(this.getDataFromUI());
            this.trigger("save", {view: this});
            return true;
        },

        deleteTodo: function () {
            this.trigger("delete", {view: this});
            return false;
        },

        cancel: function () {
            this.trigger("cancel", {view: this});
            return false;
        },

        /**
         * returns true if the user has made any change
         * @returns {Boolean}
         */
        isDirty: function () {
            var isDirty = false,
                current = this.model.clone().set(this.getDataFromUI());
            _.every(this.originalAttributes, function (value, name) {
                if (current.attributes[name] !== undefined && value.toString() !== current.attributes[name].toString()) {
                    isDirty = true;
                    return;
                }
                return true;
            }.bind(this));
            return isDirty;
        },

        close: function () {
            if (!this.model.isNew()) {
                this.trigger('endEditing', {view: this, model: this.model});
            }
            $(this.el).unbind();
            $(this.el).empty();
            $(this.el).remove();
        }

    });

    global.TodoValidationErrorsView = Backbone.View.extend({
        tagName: 'div',

        /**
         * @type {Object}
         */
        viewTemplates: null,


        initialize: function (options) {
            this.fieldsLabels = options.fieldsLabels || {};
            this.viewTemplates = options.viewTemplates || {};
        },

        render: function (validationErrors) {
            var $self = $(this.el).addClass('validationErrors'),
                that = this;
            _.each(validationErrors, function (fieldErrors, field) {
                _.each(fieldErrors, function (errorMessage, validationKey) {
                    var fieldLabel = that.fieldsLabels[field] || field;
                    $self.append($('<div class="validationError"></div>')
                        .html("<b>" + t(fieldLabel) + "</b> " + errorMessage));
                });
            });
            return this;
        }
    });
});