//Collection

/*global window */
/*global Backbone*/
/*global $*/
/*global _*/
/*global Todo*/

var global = window || {};

$(function () {
    "use strict";

    global.TodoCollection = Backbone.Collection.extend({
        model: Todo,
        initialize: function () {
            this.fetch = this._fetch.bind(this, this.fetch.bind(this));
        },

        /*
         * @param {Object} sortOrders ie: 
         * {
         *     "description": "asc",
         *     "duedate": "desc",
         * }
         * @param {Object} valueTransforms
         */
        setMultiFieldComparator: function (sortOrders, valueTransforms) {
            var newSortOrders = {}, added = 0;
            _.each(sortOrders, function (sortOrder, sortField) {
                if (["asc", "desc"].indexOf(sortOrder) !== -1) {
                    newSortOrders[sortField] = sortOrder;
                    added += 1;
                }
            });
            if (added) {
                this.comparator = this._multiFieldComparator
                    .bind(this, newSortOrders, valueTransforms || this.model.prototype.valueTransforms || {});
            } else {
                this.comparator = null;
            }
        },

        _multiFieldComparator: function (sortOrders, valueTransforms, one, another) {
            var retVal = 0;
            if (sortOrders) {
                _.every(sortOrders, function (sortOrder, sortField) {
                    var oneValue = one.get(sortField),
                        anotherValue = another.get(sortField);
                    if (valueTransforms[sortField] instanceof Function) {
                        oneValue = valueTransforms[sortField](oneValue);
                        anotherValue = valueTransforms[sortField](anotherValue);
                    }
                    if (oneValue > anotherValue) {
                        retVal = ("desc" !== sortOrder) ? 1 : -1;
                    } else if (oneValue < anotherValue) {
                        retVal = ("desc" !== sortOrder) ? -1 : 1;
                    } else {
                        //continue
                        return true;
                    }
                });
            }
            return retVal;
        },

        getInitializedModel: function (data) {
            var model = new this.model(data);
            model.urlRoot = this.url;
            if (data) {
                model.clear();
                model.set(data);
            }
            return model;
        },

        _fetch: function (superMethod, options) {
            options = _.extend(
                {
                    statusCode: {
                        401: this.trigger.bind(this, 'unauthorized', {collection: this})
                    }
                },
                options
            );
            return superMethod(options);
        }
    });
});