//Router

/*global window */
/*global Backbone*/
/*global $*/
/*global _*/
/*global TodoRouter*/
/*global TodoView*/
/*global TodoListView*/
/*global TodoCollection*/
/*global Todo*/
/*global TodoValidationErrorsView*/
/*global t*/
/*global localStorage*/
/*global Apps*/
/*jslint unparam: true*/

var global = window || {};
global.Apps = global.Apps || {};

(function () {
    "use strict";

    var global = window || {},
        replaceVars = function(t, vars) {
            _.each(vars, function (value, varname) {
                t = t.replace('!' + varname, value);
            });
            return t;
        };


    global.TodoRouter = Backbone.Router.extend({

        /**
         * css selector where the screens should load
         * @type {String}
         */
        renderTarget: null,

        /**
         * css selector where the edit should load
         * @type {String}
         */
        itemEditTarget: null,

        /**
         * @type {String}
         */
        todoResourceUrl: null,

        /**
         * @type {Array}
         */
        fieldsLabels: null,

        /**
         * url to navigate when there is no url or wrong credentials (token)
         * @type {String}
         */
        requestTokenUrl: null,

        /**
         * event name fired when a new token is received
         * @type {String}
         */
        newTokenEventName: null,

        /**
         * event name fired when the user logs-out
         * @type {String}
         */
        userLogoutEventName: null,

        /**
         * event fired when a logout is requested
         * @type {String}
         */
        logoutPreventEventName: null,

        /**
         * @type {String}
         */
        invalidateTokenEventName: null,

        /**
         * event used to start the list application
         * @type {String}
         */
        appStartEvent: null,

        /**
         * @type {Object}
         */
        viewTemplates: null,

        routes: {
            "todo/list": "list",
            "todo/new": "newTodo"
        },

        /**
         * local storage space
         */
        storage: null,

        flushStorage: function () {
            localStorage["Todo.Session"] = JSON.stringify(this.storage);
        },

        initialize: function (options) {

            Apps.Todo = this;

            //initialize from storage
            if (localStorage["Todo.Session"] !== undefined) {
                this.storage = JSON.parse(localStorage["Todo.Session"]);
            } else {
                this.storage = {};
                this.flushStorage();
            }

            //new route handlers
            this.route(/^todo\/([0-9]+)$/, "edit", this.todoDetails.bind(this));
            this.route(/^todo\/([0-9]+)\/switch$/, "switch", this.switchState.bind(this));

            //render targets
            this.renderTarget = options.renderTarget || "#content";
            this.itemEditTarget = options.itemEditTarget || '#modal-dialog';

            //api url template
            this.requestTokenUrl = options.requestTokenUrl || null;

            this.viewTemplates = options.viewTemplates || {};

            //events
            this.newTokenEventName = options.newTokenEventName || null;
            this.userLogoutEventName = options.userLogoutEventName || null;
            this.appStartEvent = options.appStartEvent || null;
            this.logoutPreventEventName = options.logoutPreventEventName || null;
            this.invalidateTokenEventName = options.invalidateTokenEventName || null;

            this.todoResourceBaseUrl = options.todoResourceBaseUrl || null;
            this.fieldsLabels = options.fieldsLabels || {};

            //bind global events if provided

            if (this.newTokenEventName) {
                $(window).bind(this.newTokenEventName, this.onNewTokenAvailable.bind(this));
            }

            if (this.userLogoutEventName) {
                $(window).bind(this.userLogoutEventName, this.onUserLogout.bind(this));
            }

            if (this.appStartEvent) {
                $(window).bind(this.appStartEvent, this.navigate.bind(this, 'todo/list', {trigger: true}));
            }

            if (this.logoutPreventEventName) {
                $(window).bind(this.logoutPreventEventName, this.onLogoutPrevent.bind(this));
            }

            if (this.invalidateTokenEventName) {
                $(window).bind(this.invalidateTokenEventName, this.onNewTokenAvailable.bind(this, {loginData: null}));
            }

            if (this.storage.loginData) {
                //delay the initialization
                this.useTokenAvailable();
            }
            this.on('onLeaveApplication', this.closeTodoViewByEvent);
        },

        closeTodoViewByEvent: function (evtData) {
            this.onPreventLeave(evtData);
            if (evtData.proceed) {
                this.closeTodoView();
            }
        },

        onPreventLeave: function (evtData) {
            if (this.todoView && this.todoView.isDirty()) {
                if (!window.confirm(t('Todo.messages.DeleteConfirm'))) {
                    evtData.proceed = false;
                }
            }
        },

        onUserLogout: function () {
            this.todoResourceUrl = null;
            this.storage.loginData = null;
            this.flushStorage();
            this.closeTodoView();
        },

        onNewTokenAvailable: function (data) {
            this.storage.loginData = data.loginData;
            this.flushStorage();
            this.useTokenAvailable();
        },

        useTokenAvailable: function () {
            this.todoResourceUrl = replaceVars(this.todoResourceBaseUrl, this.storage.loginData);
        },

        onLogoutPrevent: function (data, evtData) {
            this.onPreventLeave(evtData);
        },

        setFieldsLabels: function (headers) {
            this.fieldsLabels = headers;
        },

        list: function (fetchOptions, onUnauthorized) {
            onUnauthorized = onUnauthorized || this.onUnauthorized.bind(this);
            this.todoList = new TodoCollection();

            //if we dont have an url, then request token
            if (!this.todoResourceUrl) {
                if (this.requestTokenUrl) {
                    return this.navigate(this.requestTokenUrl, {trigger: true});
                }
                return false;
            }

            this.todoList.url = this.todoResourceUrl;
            this.todoList.on('unauthorized', onUnauthorized);
            this.todoListView = new TodoListView(
                {
                    model: this.todoList,
                    fieldsLabels: this.fieldsLabels,
                    viewTemplates: this.viewTemplates
                }
            );
            this.todoListView.on('newTodo', this.gotoNewTodo.bind(this));
            this.todoList.fetch(fetchOptions);
            $(this.renderTarget).html(this.todoListView.render().el);
            this.todoListView.delegateEvents();
        },

        onUnauthorized: function () {
            if (this.invalidateTokenEventName) {
                $(window).trigger(this.invalidateTokenEventName);
            }
        },

        /**
         * Will switch the state of the item, unless it's not loaded.
         * if the item or the list is not loaded then it will be 
         * redirected to the item edit
         * @param {Int} id
         */
        switchState: function (id) {
            if (this.todoList) {
                var model = this.todoList.get(id);
                if (model) {
                    model.once("sync", this.navigate.bind(this, 'todo/list', {trigger: false}));
                    return model.switchState().save();
                }
            }
            this.navigate('todo/' + id, {trigger: true});
        },

        /**
         * Will display a todo, no matter if the collection is loaded
         * @param {Int} id
         */
        todoDetails: function (id) {
            this.checkListIsLoadedThen(this.displayTodoById.bind(this, id));
        },

        /**
         * Executes a function after checking that 
         * list is loaded or after loading it
         * @param {type} callback
         */
        checkListIsLoadedThen: function (callback) {
            if (this.todoList) {
                //the collection is initialized
                callback.call();
            } else {
                //fetch the collection
                this.list({success: callback});
            }
        },

        /**
         * Displays a todo based on its id
         * assumes that todoList is fetched an ready to query
         * @param {Int} id
         */
        displayTodoById: function (id) {
            this.todo = this.todoList.get(id);
            if (this.todo) {
                this.displayTodo(this.todo);
            } else {
                this.todo = this.todoList.getInitializedModel({id: id});
                this.todo.fetch({success: this.displayTodoWithoutErrors.bind(this, this.todo)});
            }
        },

        gotoNewTodo: function () {
            this.navigate("todo/new", {trigger: true});
        },

        newTodo: function (todoTemplate, errorMessage) {
            this.checkListIsLoadedThen(this.displayNewTodo.bind(this, todoTemplate, errorMessage));
        },

        saveTodo: function (event) {
            var model = event.view.model,
                saved = true,
                onSaved = function () {
                    model.trigger('sync');
                    this.todoListView.relocateTodo(model);
                    this.todoListView.revealTodo(model);
                },
                first = false;

            if (model.isNew()) {
                first = !this.todoList.length;
                this.todoList.create(event.view.model, {
                    success: function (model) {
                        model.bind('sync', this.closeTodoView, this);
                        if (first) {
                            this.todoList.trigger('reset');
                        }
                        onSaved.apply(this);
                    }.bind(this),
                    error: function (model, res) {
                        this.todoList.remove(model);
                        this.todoList.trigger('reset');
                        this.displayTodoWithError(model, res);
                    }.bind(this)
                });
                //there was errors on the validation, it will not 
                if (model.validationError) {
                    this.displayTodoWithValidationErrorMessages(model, model.validationError);
                    return false;
                }
            } else {
                saved = model.save(null, {success: onSaved.bind(this), error: this.displayTodoWithError.bind(this)});
                if (!saved) {
                    this.displayTodoWithValidationErrorMessages(model, model.validationError);
                    return false;
                }
            }
            return saved;
        },

        deleteTodo: function (event) {
            if (global.confirm(t('Todo.messages.DeleteConfirm'))) {
                event.view.model.destroy({
                    success: function () {
                        this.closeTodoView();
                        this.navigate('todo/list', {trigger: !this.todoList.length});
                    }.bind(this)
                });
            }
        },

        displayNewTodo: function (todoTemplate, errorMessage) {
            this.displayTodo(todoTemplate || this.todoList.getInitializedModel(), errorMessage);
        },

        closeTodoView: function () {
            if (this.todoView) {
                this.todoView.close();
                this.todoView = null;
            }
        },

        displayTodoWithError: function (todo, resp) {
            var message, data;
            try {
                data = resp.responseJSON.data;
                switch (resp.status) {
                case 412:
                    return this.displayTodoWithValidationErrorMessages(todo, data);
                case 401:
                    return this.navigate(this.requestTokenUrl, {trigger: true});
                default:
                    message = data;
                }
            } catch (ignore) {

            }
            return this.displayTodo(todo, message);
        },

        displayTodoWithValidationErrorMessages: function (todo, validationErrorMessages) {
            var message = $(
                new TodoValidationErrorsView({
                    fieldsLabels: this.fieldsLabels,
                    viewTemplates: this.viewTemplates
                }).render(validationErrorMessages).el
            ).html();
            return this.displayTodo(todo, message);
        },

        displayTodoWithoutErrors: function (todo) {
            return this.displayTodo(todo);
        },

        displayTodo: function (todo, errorMessage) {
            var that = this, data, evtData = {proceed: true};
            this.closeTodoViewByEvent(evtData);
            if (!evtData.proceed) {
                this.navigateBack(0, {trigger: false, replace: true});
                return;
            }
            this.todoView = new TodoView({model: todo, viewTemplates: this.viewTemplates});
            $(this.itemEditTarget)
                .removeClass('animable')
                .css('top', '-' + $(this.itemEditTarget).height() + 'px')
                .css('width', this.todoListView.getWidth() + 'px');
            //new or new with errors
            if (todo.isNew() || !todo.get('id')) {
                $(this.itemEditTarget)
                    .css('top', '50px');
            }

            _.each(
                {
                    startEditing: function () {
                        $('body').addClass('editing');
                        this.todoListView.startEditing(todo.get('id'));
                        $(this.itemEditTarget)
                            .addClass('animable')
                            .css('top', (this.todoListView.getEditingElementHeight() + 50) + 'px');
                    },
                    endEditing: function () {
                        $('body').removeClass('editing');
                        this.todoListView.endEditing();
                    },
                    save: function (event) {
                        if (this.saveTodo(event)) {
                            event.view.close();
                            //this.todoListView.endEditing();
                            this.navigate('todo/list', {trigger: false});
                        }
                    },
                    'delete': this.deleteTodo,
                    cancel: function (event) {
                        event.view.close();
                        this.todoView = null;
                        //this.todoListView.endEditing();
                        this.navigate('todo/list', {trigger: false});
                    }

                },
                function (handler, eventName) {
                    this.on(eventName, handler.bind(that));
                }.bind(this.todoView)
            );

            data = {};
            if (errorMessage) {
                data.error = errorMessage;
            }
            $(this.itemEditTarget).html(this.todoView.render(data).el);
        }
    });
}());
