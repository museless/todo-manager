//Model

/*global window */
/*global Backbone*/
/*global $*/
/*global _*/
/*jshint -W009 */
/*global t*/

var global = window || {};

$(function () {
    "use strict";

    function pad(n, width, z) {
        z = z || '0';
        n = n.toString();
        return n.length >= width ? n : new [].constructor(width - n.length + 1).join(z) + n;
    }

    var defaultDate = new Date();
    defaultDate.setTime(defaultDate.getTime() + 1000 * 60 * 60 * 24 * 2);
    defaultDate = defaultDate.getFullYear().toString() + '-'
        + pad(defaultDate.getMonth() + 1, 2) + '-'
        + pad(defaultDate.getDate(), 2) + " "
        + pad(defaultDate.getHours(), 2) + ':'
        + pad(defaultDate.getMinutes(), 2) + ':'
        + pad(defaultDate.getSeconds(), 2);

    global.Todo = Backbone.Model.extend({

        initialize: function () {
            this.fetch = this._fetch.bind(this, this.fetch.bind(this));
        },

        getDatetimeLocal: function () {
            return this.get('duedate').replace(/^(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}):\d{2}$/, '$1T$2');
        },

        valueTransforms: {
            priority: function (val) { return parseInt(val, 10); },
            duedate: function (val) { return val.match(/\d/g).join(""); }
        },

        // If you return a string from the validate function,
        // Backbone will throw an error
        validate: function (attributes) {
            var hasErrors = false,
                errorMessages = {};

            //check that all fields are filled
            /*jslint unparam: true*/
            _.every(this.defaults, function (defaultValue, fieldName) {
                if (fieldName !== 'id' && (attributes[fieldName] === null || attributes[fieldName] === '' || attributes[fieldName].length === 0)) {
                    hasErrors = true;
                    errorMessages[fieldName] = {'isEmpty': t('Todo.ValidationErrors.CantBeEmpty')};
                    return;
                }
                return true;
            });
            /*jslint unparam: false*/

            if (!hasErrors) {
                //specific attributes validations
                if (attributes.description.length === 0 || attributes.description.length > 100) {
                    hasErrors = true;
                    errorMessages = {'description': {'isEmpty': t('Todo.ValidationErrors.TooMuchCharacters')}};
                } else if (!attributes.duedate.match(/^\d{4}-\d{2}-\d{2}(?:T\d{2}\:\d{2})|(?: \d{2}\:\d{2}\:\d{2})$/)) {
                    hasErrors = true;
                    errorMessages = {'duedate': {'Regex': t('Todo.ValidationErrors.MustBeDate')}};
                } else if (!attributes.priority.match(/^\d+$/)) {
                    hasErrors = true;
                    errorMessages = {'priority': {'Regex': t('Todo.ValidationErrors.MustBeNumber')}};
                } else if (parseInt(attributes.priority, 10) < 0 || parseInt(attributes.priority, 10) > 10) {
                    hasErrors = true;
                    errorMessages = {'priority': {'Regex': t('Todo.ValidationErrors.MustBeNumberBetween', {min: 0, max: 10})}};
                }
            }

            if (hasErrors) {
                this._lastValidationResult = errorMessages;
                return errorMessages;
            }
            this._lastValidationResult = null;
        },

        /**
         * set the default data for new objects
         */
        defaults: {
            "id": null,
            "description": "type the todo description here",
            "completed": 0,
            "duedate": defaultDate,
            "priority": 5
        },

        /**
         * Will change the state, without saving
         * @returns {Todo}
         */
        switchState: function () {
            this.set('completed', this.get('completed') === '1' ? 0 : 1);
            return this;
        },

        /**
         * Returns the duedate in the locale readable format
         * @returns {String}
         */
        getDuedateLocaleString: function () {
            return new Date(this.get('duedate')).toLocaleString();
        },

        /**
         * Returns the date as it will be shown on the interface
         * @returns {Object}
         */
        getReadableLocalizedJSON: function () {
            return _.extend(this.toJSON(), {
                duedate: this.getDuedateLocaleString()
            });
        },

        _fetch: function (superMethod, options) {
            options = _.extend(
                {
                    statusCode: {
                        401: this.trigger.bind(this, 'unauthorized', {model: this})
                    }
                },
                options
            );
            return superMethod(options);
        }
    });
});