<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Todo\Controller\Todo' => 'Todo\Controller\TodoController',
            'Todo\Controller\TodoRestApi' => 'Todo\Controller\TodoRestApiController',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'ValidateApiToken' => 'Todo\Controller\Plugin\ValidateApiToken',
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'todo' => 'Todo\View\Helper\Todo',
         )
    ),
    'router' => array(
        'routes' => array(
            'todo' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/todo[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Todo\Controller\Todo',
                        'action'     => 'index',
                    ),
                ),
            ),
            'todo-api-description' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/todo-api/v1.0',
                    'constraints' => array(
                    ),
                    'defaults' => array(
                        'controller' => 'Todo\Controller\Todo',
                        'action'     => 'apiDescription',
                    ),
                ),
            ),
            'todo-api' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/todo-api/v1.0/[:user_id]/[:token][/:id]',
                    'constraints' => array(
                        'user'  => '[0-9]+',
                        'token'  => '[0-9A-Za-z]+',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Todo\Controller\TodoRestApi',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'todo' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
