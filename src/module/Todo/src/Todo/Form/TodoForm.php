<?php
namespace Todo\Form;

use Zend\Form\Form;

class TodoForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('todo');
        $this->setAttribute('method', 'post');
        $this->add(
            array(
                'name' => 'id',
                'attributes' => array(
                    'type'  => 'hidden',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'description',
                'attributes' => array(
                    'type'  => 'text',
                ),
                'options' => array(
                    'label' => 'Description',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'completed',
                'attributes' => array(
                    'type'  => 'int',
                ),
                'options' => array(
                    'label' => 'Completed',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'duedate',
                'attributes' => array(
                    'type'  => 'timestamp',
                ),
                'options' => array(
                    'label' => 'Due date',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'priority',
                'attributes' => array(
                    'type'  => 'int',
                ),
                'options' => array(
                    'label' => 'Priority',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'submit',
                'attributes' => array(
                    'type'  => 'submit',
                    'value' => 'Go',
                    'id' => 'submitbutton',
                ),
            )
        );
    }
}