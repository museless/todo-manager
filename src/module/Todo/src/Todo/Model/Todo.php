<?php
namespace Todo\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Todo implements InputFilterAwareInterface
{
    public $id;
    public $user_id;
    public $description;
    public $completed;
    public $duedate;
    public $priority;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id'])) ? $data['id'] : null;
        $this->user_id     = (isset($data['user_id'])) ? $data['user_id'] : null;
        $this->description = (isset($data['description'])) ? $data['description'] : null;
        $this->completed  = (isset($data['completed'])) ? $data['completed'] : null;
        $this->duedate = (isset($data['duedate'])) ? $data['duedate'] : null;
        $this->priority  = (isset($data['priority'])) ? $data['priority'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    /**
     * returns an associative array with the values
     * @return array
     */
    public function getData()
    {
        $me = $this;
        $publics = function() use ($me) { 
            return get_object_vars($me); 
        }; 
        return $publics();
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'id',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'Int'),
                        ),
                        'validators' => array(
                            array(
                                'name'    => 'Regex',
                                'options' => array(
                                    'pattern' => '/^\\d+$/',
                                    'message' => "must be a number",
                                ),
                            ),
                        ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'description',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name'    => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min'      => 1,
                                    'max'      => 100,
                                ),
                            ),
                        ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'completed',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name'    => 'Regex',
                                'options' => array(
                                    'pattern' => '/^[01]$/',
                                    'message' => "must be zero or one (0, 1)",
                                ),
                            ),
                        ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'duedate',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name'    => 'Regex',
                                'options' => array(
                                    'pattern' => 
                                        '/^\\d{4}-\\d{2}-\\d{2}(?:T\\d{2}\\:\\d{2})|(?: \\d{2}\\:\\d{2}\\:\\d{2})$/',
                                    'message' => 
                                        "must be a date in the format yyyy-mm-dd hh:mm:ss (or yyyy-mm-ddThh:mm)",
                                ),
                            ),
                        ),
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'priority',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name'    => 'Regex',
                                'options' => array(
                                    'pattern' => '/^\\d+$/',
                                    'message' => "must be a number",
                                ),
                            ),
                            array(
                                'name'    => 'Between',
                                'options' => array(
                                    'min' => 1,
                                    'max' => 10,
                                    'message' => "must be a number between 0 and 10",
                                ),
                            ),
                        ),
                    )
                )
            );
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}