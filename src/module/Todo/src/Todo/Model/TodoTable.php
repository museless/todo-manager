<?php

namespace Todo\Model;

use Zend\Db\TableGateway\TableGateway;

class TodoTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Retrieves all todos in the database
     * @return Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll($user_id)
    {
        $resultSet = $this->tableGateway->select(array('user_id' => $user_id));
        return $resultSet;
    }

    /**
     * retrieves a single todo From the database
     * @param int $id
     * @return object
     * @throws \Exception when cant find the item
     */
    public function getTodo($id, $user_id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id, 'user_id' => $user_id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Saves a todo in the database
     * @param \Todo\Model\Todo $todo
     * @return int id of the inserted todo
     * @throws \Exception when the id does not match a todo
     */
    public function saveTodo(Todo $todo, $user_id)
    {
        $data = array(
            'description' => $todo->description,
            'completed'  => $todo->completed,
            'duedate'  => $todo->duedate,
            'priority'  => $todo->priority,
        );

        $id = (int) $todo->id;
        if ($id == 0) {
            $data['user_id'] = $user_id;
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->getTodo($id, $user_id)) {
                $this->tableGateway->update($data, array('id' => $id, 'user_id' => $user_id));
            } else {
                throw new \Exception('Todo id does not exist');
            }
        }
        return $id;
    }

    /**
     * deletes a todo from the database
     * @param int $id
     */
    public function deleteTodo($id, $user_id)
    {
        $this->tableGateway->delete(array('id' => $id, 'user_id' => $user_id));
    }
}