<?php

namespace Todo\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Renderer\RendererInterface;
use Zend\View\Helper\HelperInterface;

/**
 * adds the necessary scripts and return the login templates
 */
class Todo extends AbstractHelper
    implements HelperInterface
{
    /**
     * translations required for the todo application
     * @var Array
     */
    private $_translationsTexts = array(
        'Todo.ValidationErrors.CantBeEmpty' => 'Value is required and can\'t be empty',
        'Todo.ValidationErrors.TooMuchCharacters' => 'Cannot have more than 100 characters',
        'Todo.ValidationErrors.MustBeDate' => 
            'Must be a date in the format yyyy-mm-dd hh:mm:ss (or yyyy-mm-ddThh:mm)',
        'Todo.ValidationErrors.MustBeNumber' => 'must be a number',
        'Todo.ValidationErrors.MustBeNumberBetween' => 'must be a number between {!min} and {!max}',
        'Todo.fieldLabels.Id' => "Id",
        'Todo.fieldLabels.Description' => "Description",
        'Todo.fieldLabels.Completed' => "Completed",
        'Todo.fieldLabels.Duedate' => "Duedate",
        'Todo.fieldLabels.Priority' => "Priority",
        'Todo.formLabel.Save' => 'Save',
        'Todo.formLabel.Delete' => 'Delete',
        'Todo.formLabel.Create' => 'Create',
        'Todo.formLabel.Cancel' => 'Cancel',
        'Todo.messages.DeleteConfirm' => 'Are you sure you wish to delete the item?',
        'Todo.messages.NothingToDo' => 'Seems like you dont have nothing to do yet.',
        'Todo.messages.WantToAddNew' => 'Would you want to add a new TODO?',
        'Todo.messages.AddNewTodo' => 'Add New Todo',
        'Todo.messages.DeleteConfirm' => 'you have unsaved changes. Are you sure you want to continue?.'
    );

    /**
     *
     * @var RendererInterface
     */
    private $_view;
    
    /**
     * adds the necessary scripts and return the login templates
     * @param  string $hashUrlTo url required (login, register, recover)
     * @return string login templates
     */
    public function __invoke($hashUrlTo = null)
    {
        if (isset($hashUrlTo)) {
            return $this->getHashUrlTo($hashUrlTo);
        }
        return $this;
    }

    public function getView()
    {
        return $this->_view;
    }
    
    public function setView(RendererInterface $view)
    {
        $this->_view = $view;
    }

    /**
     * returns the link hash for the requested item
     * @param string $hashUrlTo
     * @return string
     */
    public function getHashUrlTo($hashUrlTo)
    {
        return "#todo/" . $hashUrlTo;
    }

    /**
     * return the translated texts
     * @return array
     */
    public function getTranslations()
    {
        $texts = array();

        foreach ($this->_translationsTexts as $key => $value) {
            $texts[$key] = $this->_view->translate($key);
            if ($texts[$key] === $key) {
                $texts[$key] = $value;
            }
        }

        return $texts;
    }

    /**
     * Adds the javascript and styles required for the application
     */
    public function includeHeadScripts()
    {
        $this->_view->headScript()
            ->appendFile($this->_view->basePath() . '/js/todo/model.js')
            ->appendFile($this->_view->basePath() . '/js/todo/collection.js')
            ->appendFile($this->_view->basePath() . '/js/todo/view.js')
            ->appendFile($this->_view->basePath() . '/js/todo/router.js');
        $this->_view->headLink()
            ->appendStylesheet($this->_view->basePath() . '/css/todo/todo.css');
    }

    /**
     * returns the merged options for the plugin
     */
    private function _getOptions($options)
    {
        return array_merge(
            $options, 
            array(
                'fieldsLabels' => array(
                    'id' => 'Todo.fieldLabels.Id',
                    'description' => 'Todo.fieldLabels.Description',
                    'completed' => 'Todo.fieldLabels.Completed',
                    'duedate' => 'Todo.fieldLabels.Duedate',
                    'priority' => 'Todo.fieldLabels.Priority'
                ),
                'todoResourceBaseUrl' => $this->_view->url(
                    'todo-api', 
                    array(
                        'user_id' => '!user_id',
                        'token' => '!token'
                    )
                )
            )
        );
    }
    
    /**
     * returns the html required for the application.
     * @param array $options
     */
    public function getApp($options)
    {
        return $this->_view->partial(
            'todo/helper/app.phtml', 
            array(
                'options' => $this->_getOptions($options),
                'translations' => $this->getTranslations(),
            )
        );
    }
}