<?php
namespace Todo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Todo\Model\Todo;
use Todo\Form\TodoForm;

class TodoController extends AbstractActionController
{
    protected $todoTable;
    
    /**
     * @var Todo\Form\TodoForm
     */
    private $_form;
    
    public function getTodoTable()
    {
        if (!$this->todoTable) {
            $sm = $this->getServiceLocator();
            $this->todoTable = $sm->get('Todo\Model\TodoTable');
        }
        return $this->todoTable;
    }
    
    public function setTodoTable($todoTable)
    {
        $this->todoTable = $todoTable;
    }
    
    public function manageAction()
    {
        return;
    }
    
    public function apiDescriptionAction()
    {
        return new ViewModel();
    }
    
    public function indexAction()
    {
        return new ViewModel(
            array(
                'todos' => $this->getTodoTable()->fetchAll(),
            )
        );
    }
    
    /**
     * @param boolean $edit
     * @return Todo\Form\TodoForm
     */
    private function _getForm($edit = false)
    {
        if (!isset($this->_form)) {
            $this->_form = new TodoForm();
            if ($edit) {
                $this->_form->get('submit')->setValue('Edit');
            } else {
                $this->_form->get('submit')->setValue('Add');
                $this->_form->setAttribute('action', $this->url()->fromRoute('todo', array('action' => 'addPost')));
            }
        }
        return $this->_form;
    }

    public function addAction()
    {
        return array('form' => $this->_getForm());
    }
    
    public function addPostAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $todo = new Todo();
            $form = $this->_getForm();
            $form->setInputFilter($todo->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $todo->exchangeArray($form->getData());
                $this->getTodoTable()->saveTodo($todo);

                // Redirect to list of todos
                return $this->redirect()->toRoute('todo');
            }
        }
    }
    
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'todo', array(
                    'action' => 'add'
                )
            );
        }
        $todo = $this->getTodoTable()->getTodo($id);

        $form  = $this->_getForm(true);
        $form->bind($todo);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($todo->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getTodoTable()->saveTodo($form->getData());

                // Redirect to list of todos
                return $this->redirect()->toRoute('todo');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('todo');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getTodoTable()->deleteTodo($id);
            }

            // Redirect to list of todos
            return $this->redirect()->toRoute('todo');
        }

        return array(
            'id'    => $id,
            'todo' => $this->getTodoTable()->getTodo($id)
        );
    }

}
