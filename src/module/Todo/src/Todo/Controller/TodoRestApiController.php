<?php

namespace Todo\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\MvcEvent;
use Todo\Model\Todo;
use Todo\Form\TodoForm;
use Todo\Model\TodoTable;
use Zend\Config\Config;
use Zend\View\Model\JsonModel;

class TodoRestApiController extends AbstractRestfulController
{
    const MSG_ERROR_500 = 'There was an error during the processing, please try again.';
    const MSG_ERROR_401 = 'Authorization has been refused for those credentials, user_id / token.';
    
    /**
     * Todo table instance
     * @var Todo\Model\TodoTable
     */
    protected $todoTable;
    
    /**
     * @var Zend\Config\Config
     */
    private $apiConfig = null;

    /**
     * Handle the request
     *
     * @todo   try-catch in "patch" for patchList should be removed in the future
     * @param  MvcEvent $e
     * @return mixed
     * @throws Exception\DomainException if no route matches in event or invalid HTTP method
     */
    public function onDispatch(MvcEvent $e)
    {
        $ret = $this->_predispatch($e);
        if (isset($ret)) {
            $e->setResult($ret);
            return $ret;
        }

        //catch any exception on the request, and give a generic JSON error message
        try {
            $ret = parent::onDispatch($e);
        } catch(\Exception $err) {
            $this->response->setStatusCode(500);/* Internal Server Error */
            $e->setResult(
                new JsonModel(array('data' => $err->getMessage(). self::MSG_ERROR_500))
            );
        }
        return $ret;
    }
    
    public function getApiConfig()
    {
        if (!$this->apiConfig) {
            $config = new Config($this->getServiceLocator()->get('Config'));
            $this->apiConfig = $config->todoApi;
        }
        return $this->apiConfig;
    }
    
    private function _predispatch(MvcEvent $e)
    {
        $user_id = $this->params("user_id");
        $token = $this->params("token");
        $config = $this->getApiConfig();
        
        if (!$this->validateApiToken($user_id, $token, $config)) {
            $this->response->setStatusCode(401);/* Unauthorized */
            return new JsonModel(
                array(
                    'data' => self::MSG_ERROR_401
                )
            );
        }
    }
    

    /**
     * returns the todo table instance
     * @return Todo\Model\TodoTable
     */
    public function getTodoTable()
    {
        if (!$this->todoTable) {
            $sm = $this->getServiceLocator();
            $this->todoTable = $sm->get('Todo\Model\TodoTable');
        }
        return $this->todoTable;
    }
    
    /**
     * Setst the todo table instance
     * @param Todo\Model\TodoTable $todoTable
     */
    public function setTodoTable($todoTable)
    {
        $this->todoTable = $todoTable;
    }
    
    public function getList()
    {
        $results = $this->getTodoTable()->fetchAll($this->params("user_id"));
        $data = array();
        foreach ($results as $result) {
            $data[] = $result;
        }

        return new JsonModel($data);
    }

    public function get($id)
    {
        $todo = $this->getTodoTable()->getTodo($id, $this->params("user_id"));

        return new JsonModel($todo->getData());
    }
    
    public function create($data)
    {
        $form = new TodoForm();
        $todo = new Todo();
        
        $form->setInputFilter($todo->getInputFilter()->remove('id'));
        $form->setData($data);
        
        if ($form->isValid()) {
            $todo->exchangeArray($form->getData());
            $id = $this->getTodoTable()->saveTodo($todo, $this->params("user_id"));
        } else {
            $messages = $form->getMessages();
            $this->response->setStatusCode(412);
            return new JsonModel(array('data' => $messages));
        }

        return $this->get($id);
    }

    public function update($id, $data)
    {
        $data['id'] = $id;
        $todo = $this->getTodoTable()->getTodo($id, $this->params("user_id"));
        $form  = new TodoForm();
        $form->bind($todo);
        $form->setInputFilter($todo->getInputFilter());
        $form->setData($data);
        if ($form->isValid()) {
            $id = $this->getTodoTable()->saveTodo($form->getData(), $this->params("user_id"));
        } else {
            $messages = $form->getMessages();
            $this->response->setStatusCode(412);
            return new JsonModel(array('data' => $messages));
        }
        
        return $this->get($id);
    }

    public function delete($id)
    {
        $this->getTodoTable()->deleteTodo($id, $this->params("user_id"));

        return new JsonModel(
            array(
                'data' => 'deleted',
            )
        );
    }

}