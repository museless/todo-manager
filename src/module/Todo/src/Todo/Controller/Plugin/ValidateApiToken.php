<?php

namespace Todo\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class ValidateApiToken extends AbstractPlugin
{
    /**
     * Validates a pair of user_id and token
     *
     * @param string $user_id
     * @param string $token
     * @return mixed
     */
    public function __invoke($user_id, $token, $config)
    {
        return $this->validate($user_id, $token, $config);
    }
    
    /**
     * validates a pair of user_id and token
     * @param string $user_id
     * @param string $token
     * @return boolean
     */
    public function validate($user_id, $token, $config)
    {
        //return trim(`cat /tmp/auth`) == '1';
        $currTime = time();
        $currSize = strval(strlen($currTime));
        if (strpos($token, $currSize) === 0) {
            $maxTime = intval(substr($token, strlen($currSize), $currSize));
            if ($maxTime > $currTime) {
                $tokenValue = substr($token, strlen($currSize) + $currSize);

                $digest = sha1($config->secret . $maxTime. $user_id);
                if ($tokenValue === $tokenValue) {
                    return true;
                }
            }
        }
        return false;
    }
}