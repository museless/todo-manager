<?php
namespace UserTest\Model;

use User\Model\User;
use PHPUnit_Framework_TestCase;

class UserTest extends PHPUnit_Framework_TestCase
{
    public function testUserInitialState()
    {
        $user = new User();

        $this->assertNull($user->firstname, '"firstname" should initially be null');
        $this->assertNull($user->id, '"id" should initially be null');
        $this->assertNull($user->lastname, '"lastname" should initially be null');
        $this->assertNull($user->email, '"email" should initially be null');
        $this->assertNull($user->password, '"password" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $user = new User();
        $data  = array(
            'firstname' => 'some firstname',
            'id'     => 123,
            'lastname'  => 'some lastname',
            'email'  => 'some email',
            'password'  => 'some password'
        );

        $user->exchangeArray($data);

        $this->assertSame($data['firstname'], $user->firstname, '"firstname" was not set correctly');
        $this->assertSame($data['id'], $user->id, '"id" was not set correctly');
        $this->assertSame($data['lastname'], $user->lastname, '"lastname" was not set correctly');
        $this->assertSame($data['email'], $user->email, '"email" was not set correctly');
        $this->assertSame($data['password'], $user->password, '"password" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $user = new User();

        $user->exchangeArray(
            array(
                'firstname' => 'some firstname',
                'id'     => 123,
                'lastname'  => 'some lastname'
            )
        );
        $user->exchangeArray(array());

        $this->assertNull($user->firstname, '"firstname" should have defaulted to null');
        $this->assertNull($user->id, '"id" should have defaulted to null');
        $this->assertNull($user->lastname, '"lastname" should have defaulted to null');
        $this->assertNull($user->email, '"email" should have defaulted to null');
        $this->assertNull($user->password, '"password" should have defaulted to null');
    }
}
