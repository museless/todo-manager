<?php
namespace UserTest\Controller;

use UserTest\Bootstrap;
use User\Controller\UserController;
use User\Controller\Plugin\CreateApiToken;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use PHPUnit_Framework_TestCase;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\View\Model\JsonModel;
use User\Model\UserTable;
use User\Model\User;
use Zend\Config\Config;

class UserControllerTest extends AbstractHttpControllerTestCase
{
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    protected function setUp()
    {
        /**
         * mock the token creator
         */
        $createApiToken = $this->getMock(
            'User\Controller\Plugin\CreateApiToken',
            array('create'), array(), '', false
        );
        
        $createApiToken->expects($this->any())
            ->method('create')
            ->will($this->returnValue(array('token'=>'mockedtoken', 'user_id'=>'mockeduserid')));

        /**
         * mock the user table and model
         */
        $mockUserTable = $this->getMock(
            'User\Model\UserTable',
            array('getUserByEmailAndPassword'), array(), '', false
        );
        
        $mockUserModel = $this->getMock(
            'User\Model\User',
            array('getUserByEmailAndPassword'), array(), '', false
        );
        
        $map = array(
            array('test@test.com', md5('test'), $mockUserModel),
            array('wrong@test.com', md5('wrong'), null)
        );

        $mockUserTable->expects($this->any())
            ->method('getUserByEmailAndPassword')
            ->will($this->returnValueMap($map));
        
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = $this->getMock(
            'User\Controller\UserController',
            array('getUserTable'), array(), '', false
        );
        $this->controller->expects($this->any())
            ->method('getUserTable')
            ->will($this->returnValue($mockUserTable));
        
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'index'));
        $this->event      = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);
        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->getPluginManager()
           ->setService('createApiToken', $createApiToken);
        $this->controller->setServiceLocator($serviceManager);
        $this->setApplicationConfig(
            include 'TestConfig.php.dist'
        );
        parent::setUp();
    }

    public function testLoginActionCanBeAccessedAndShouldReturnToken()
    {
        $this->routeMatch->setParam('action', 'login');

        $this->request->setMethod('post');
        $this->request->getPost()->set('user_email', 'test@test.com');
        $this->request->getPost()->set('user_password', 'test');
        
        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertResponseStatusCode(200);
        $this->assertInstanceOf('Zend\View\Model\JsonModel', $this->event->getResult());
        $this->assertEquals(
            'mockedtoken',
            $this->event->getResult()->token, 
            "should return the api token"
        );
        $this->assertEquals(
            'mockeduserid',
            $this->event->getResult()->user_id, 
            "should return the user id"
        );
    }

    public function testLoginActionWillFailWithInvalidCredentials()
    {
        $this->routeMatch->setParam('action', 'login');

        $this->request->setMethod('post');
        $this->request->getPost()->set('user_email', 'wrong@test.com');
        $this->request->getPost()->set('user_password', 'wrong');
        
        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(403, $response->getStatusCode());
    }

    public function testLoginActionFailsWithNoPostData()
    {
        $this->routeMatch->setParam('action', 'login');

        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(412, $response->getStatusCode());
    }

    public function testRegisterActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'register');

        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }
}
