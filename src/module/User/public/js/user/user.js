//Router

/*global window */
/*global Backbone*/
/*global $*/
/*global _*/
/*global t*/
/*jslint unparam: true*/
/*global localStorage*/
/*global Apps*/
/*global TodoCollection*/

var global = window || {};
global.Apps = global.Apps || {};

(function () {
    "use strict";

    var UserBaseView = null, UserLoginView = null, UserRegisterView = null, UserRecoverView = null, ValidationErrorsView = null,
        UserLoginModel = null, UserRegisterModel = null, UserRecoverModel = null,
        renderValidationErrors = function (validationErrors, fieldsLabels) {
            return new ValidationErrorsView().render(validationErrors, fieldsLabels).el;
        },
        validateEmail = function (email) {
            var re = new RegExp("^(([^<>()[\\]\\\\.,;:\\s@\\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
            return re.test(email);
        },
        validateIsAlpha = function (val) {
            return (/^[A-Za-z\s]+$/i).test(val);
        };

    global.UserRouter = Backbone.Router.extend({

        /**
         * css selector where the screens should load
         * @type {String}
         */
        renderTarget: null,

        /**
         * template to render the login
         * @type {String}
         */
        templateUserLogin: null,

        /**
         * template to render the registration
         * @type {String}
         */
        templateUserRegister: null,

        /**
         * template to render the recover
         * @type {String}
         */
        templateUserRecover: null,

        /**
         * @type {UserLoginView}
         */
        userLoginView: null,

        /**
         * @type {UserRegisterView}
         */
        userRegisterView: null,

        /**
         * @type {UserRecoverView}
         */
        userRecoverView: null,

        /**
         * @type {String}
         */
        logoutReturnUrl: null,

        /**
         * @type {String}
         */
        loginUrl: null,

        /**
         * @type {String}
         */
        registerUrl: null,

        /**
         * @type {String}
         */
        recoverUrl: null,

        /**
         * @type {Object}
         */
        fieldsLabels: null,

        /**
         * @type {String}
         */
        logoutEventName: null,

        /**
         * @type {Array}
         */
        loginEventName: null,

        /**
         * @type {String}
         */
        logoutPreventEventName: null,

        /**
         * @type {String}
         */
        invalidateTokenEventName: null,

        /**
         * keep track of the login state
         */
        currentState: 'uninitialized',

        routes: {
            "user/login": "login",
            "user/register": "register",
            "user/recover": "recover",
            "user/logout": "onLogout"
        },

        /**
         * local storage space
         */
        storage: null,

        flushStorage: function () {
            localStorage["User.Session"] = JSON.stringify(this.storage);
        },

        /**
         * Changes the login state
         * @param {String} state
         */
        changeState: function (state) {
            $('body').removeClass('user-login-state-' + this.storage.state);
            this.storage.state = state;
            this.flushStorage();
            $('body').addClass('user-login-state-' + state);
        },

        initialize: function (options) {
            options = options || {};

            Apps.User = this;

            //initialize from storage
            if (localStorage["User.Session"] !== undefined) {
                this.storage = JSON.parse(localStorage["User.Session"]);
            } else {
                this.storage = {};
                this.flushStorage();
            }

            this.renderTarget = options.renderTarget || "#content";
            this.fieldsLabels = options.fieldsLabels || {};

            //global events
            this.loginEventName = options.loginEventName || null;
            if (this.loginEventName && !this.loginEventName instanceof [].constructor) {
                this.loginEventName = [this.loginEventName];
            }
            this.logoutEventName = options.logoutEventName || null;
            if (this.logoutEventName && !this.logoutEventName instanceof [].constructor) {
                this.logoutEventName = [this.logoutEventName];
            }
            this.logoutPreventEventName = options.logoutPreventEventName || null;
            this.invalidateTokenEventName = options.invalidateTokenEventName || null;

            //Initialize views
            this.templateUserLogin = options.templateUserLogin;
            this.templateUserRegister = options.templateUserRegister;
            this.templateUserRecover = options.templateUserRecover;
            this.userLoginView = new UserLoginView({template: this.templateUserLogin, model: new UserLoginModel()});
            this.userRegisterView = new UserRegisterView({template: this.templateUserRegister, model: new UserRegisterModel()});
            this.userRecoverView = new UserRecoverView({template: this.templateUserRecover, model: new UserRecoverModel()});

            //url configuration
            this.logoutReturnUrl = options.logoutReturnUrl || null;
            this.loginUrl = options.loginUrl;
            this.registerUrl = options.registerUrl;
            this.recoverUrl = options.recoverUrl;


            //bind view events
            this.userLoginView.on('onLogin', this.doLogin.bind(this));
            this.userRegisterView.on('onRegister', this.doRegister.bind(this));
            this.userRecoverView.on('onRecover', this.doRecover.bind(this));

            if (this.invalidateTokenEventName) {
                $(window).bind(this.invalidateTokenEventName, this.changeState.bind(this, 'unlogged'));
            }

            if (this.storage.state) {
                this.changeState(this.storage.state);
            } else {
                this.changeState('unlogged');
            }

            //delayed start, allow other applications to being instanced
            if (this.storage.state === 'logged') {
                if (this.invalidateTokenEventName) {
                    window.setTimeout(function () {
                        var todoList = new TodoCollection(), todo;
                        todoList.url = Apps.Todo.todoResourceUrl;
                        todo = todoList.getInitializedModel({id: 0});
                        todo.on('unauthorized', function () {
                            $(window).trigger(this.invalidateTokenEventName);
                        }.bind(this)).fetch();
                    }.bind(this));
                }
            }
        },

        /**
         * renders the login view
         */
        login: function () {
            $(this.renderTarget).html(this.userLoginView.render().el);
            $(this.renderTarget).find('input:first').focus();
            this.userLoginView.delegateEvents();
        },

        /**
         * renders the register view
         */
        register: function () {
            $(this.renderTarget).html(this.userRegisterView.render().el);
            $(this.renderTarget).find('input:first').focus();
            this.userRegisterView.delegateEvents();
        },

        /**
         * renders the recover view
         */
        recover: function () {
            $(this.renderTarget).html(this.userRecoverView.render().el);
            $(this.renderTarget).find('input:first').focus();
            this.userRecoverView.delegateEvents();
        },

        triggerLoginEventName: function (data) {
            if (this.loginEventName) {
                _.each(this.loginEventName, function (eventName) {
                    var evt = $.Event(eventName);
                    evt.loginData = data;
                    $(window).trigger(evt);
                });
            }
            this.changeState("logged");
        },

        /**
         * handles the login action
         */
        doLogin: function () {
            window.console.log("doLogin");

            var userModel = this.userLoginView.getUserData(),
                validationErrors = userModel.validateFields();
            if (validationErrors) {
                this.userLoginView.displayErrors(renderValidationErrors(validationErrors, this.fieldsLabels));
            } else {
                this.userLoginView.clearErrors();
                $.ajax({
                    url: this.loginUrl,
                    data: userModel.attributes,
                    type: "POST",
                    success: function (data) {
                        this.triggerLoginEventName(data);
                    }.bind(this),
                    error: function (xhr) {
                        this.userLoginView.displayErrors(t(xhr.responseJSON.data));
                    }.bind(this),
                    dataType: 'json'
                });
            }

            window.console.log(userModel.attributes);
        },

        /**
         * handles the register action
         */
        doRegister: function () {
            window.console.log("doRegister");

            var userModel = this.userRegisterView.getUserData(),
                validationErrors = userModel.validateFields();
            if (validationErrors) {
                this.userRegisterView.displayErrors(renderValidationErrors(validationErrors, this.fieldsLabels));
            } else {
                this.userRegisterView.clearErrors();
                $.ajax({
                    url: this.registerUrl,
                    data: userModel.attributes,
                    type: "POST",
                    success: function (data) {
                        this.triggerLoginEventName(data);
                    }.bind(this),
                    error: function (xhr) {
                        switch (xhr.status) {
                        case 412:
                            if (xhr.responseJSON.data instanceof Object) {
                                return this.userRegisterView.displayErrors(renderValidationErrors(xhr.responseJSON.data, this.fieldsLabels));
                            }
                            break;
                        }
                        this.userLoginView.displayErrors(t(xhr.responseJSON.data));
                    }.bind(this),
                    dataType: 'json'
                });
            }

            window.console.log(userModel.attributes);
        },

        /**
         * handles the recover action
         */
        doRecover: function () {
            window.console.log("doRecover");

            var userModel = this.userRecoverView.getUserData(),
                validationErrors = userModel.validateFields();
            if (validationErrors) {
                this.userRecoverView.displayErrors(renderValidationErrors(validationErrors, this.fieldsLabels));
            } else {
                this.userRecoverView.clearErrors();
            }

            window.console.log(userModel.attributes);
        },

        /**
         * handles the logout action
         * @param {Boolean} if it's triggered from the code or the user
         */
        onLogout: function (automated) {
            var evtData = {userTriggered: !automated, proceed: true};
            window.console.log("doLogout");
            if (this.logoutPreventEventName) {
                $(window).trigger(this.logoutPreventEventName, evtData);
            }
            if (evtData.proceed) {
                if (this.logoutEventName) {
                    if (evtData.proceed) {
                        $(window).trigger(this.logoutEventName, evtData);
                        this.changeState('unlogged');
                    }
                }
                if (this.logoutReturnUrl) {
                    this.navigate(this.logoutReturnUrl, {trigger: true});
                }
            } else {
                this.navigate(
                    Backbone.history.lookupFragmentHistory(1),
                    {trigger: false, replace: true}
                );
            }
        }

    });

    UserRegisterModel = Backbone.Model.extend({
        validateField: {
            "user_firstname": true,
            "user_lastname": true,
            "user_email": true,
            "user_password": true,
            "user_password_confirmation": true
        },

        validateFields: function (onlyFirst) {
            return this.validate(this.attributes, onlyFirst instanceof Boolean ? onlyFirst : true);
        },

        validate: function (attributes, onlyFirst) {
            var hasErrors = false,
                errorMessages = {};

            //check that all fields are filled
            /*jslint unparam: true*/
            _.every(this.validateField, function (validateField, fieldName) {
                var value = attributes[fieldName],
                    fieldErrorMessages = errorMessages[fieldName] || {};

                errorMessages[fieldName] = fieldErrorMessages;

                if (!value || value === '' || value.length === 0) {
                    hasErrors = true;
                    errorMessages[fieldName].isEmpty = "User.ValidationErrors.CantBeEmpty";
                    if (onlyFirst) {
                        return;
                    }
                }

                if (hasErrors && onlyFirst) {
                    return;
                }

                if (validateField) {
                    switch (fieldName) {
                    case 'user_email':
                        if (!validateEmail(value)) {
                            fieldErrorMessages.isNotEmail = "User.ValidationErrors.MustBeEmail";
                            hasErrors = true;
                        }
                        break;
                    case 'user_firstname':
                    case 'user_lastname':
                        if (!validateIsAlpha(value)) {
                            fieldErrorMessages.isNotAphabetic = "User.ValidationErrors.MustBeAlphabetic";
                            hasErrors = true;
                        }
                        break;
                    case 'user_password':
                        if (value.length < 8) {
                            fieldErrorMessages.isTooShort = "User.ValidationErrors.PasswordTooShort";
                            hasErrors = true;
                        } else if (!(new RegExp("(^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$)")).test(value)) {
                            fieldErrorMessages.isNotMixed = "User.ValidationErrors.MixedPassword";
                            hasErrors = true;
                        }
                        break;
                    case 'user_password_confirmation':
                        if (value !== attributes.user_password) {
                            fieldErrorMessages.doesNotMatch = "User.ValidationErrors.PasswordConfirmation";
                            hasErrors = true;
                        }
                        break;
                    }
                }
                if (hasErrors) {
                    if (onlyFirst) {
                        return;
                    }
                } else {
                    delete errorMessages[fieldName];
                }
                return true;
            });
            /*jslint unparam: false*/


            if (hasErrors) {
                return errorMessages;
            }
        }
    });

    UserLoginModel = UserRegisterModel.extend({
        validateField: {
            "user_email": true,
            "user_password": false
        }
    });

    UserRecoverModel = UserRegisterModel.extend({
        validateField: {
            "user_email": true
        }
    });

    UserBaseView = Backbone.View.extend({

        /**
         * fills the user data from the view and returns a model.
         * @returns {UserRegisterModel}
         */
        getUserData: function () {
            var $self = $(this.el),
                that = this;

            this.model.clear();

            _.each(this.model.validateField, function (validateField, fieldName) {
                var value = $self.find('#' + fieldName).val();
                that.model.set(fieldName, value);
            });

            return this.model;
        },

        /**
         * clears the error message area
         */
        clearErrors: function () {
            $(this.el).find('.errors-container').empty();
        },

        /**
         * @param {HtmlElement} errors
         */
        displayErrors: function (errors) {
            $(this.el).find('.errors-container').html(errors);
        }

    });

    //User Register View
    UserRegisterView = UserBaseView.extend({
        /**
         * template renderer
         * @type {Function}
         */
        template: null,

        events: {
            "click button.register": 'onRegister'
        },

        initialize: function (options) {
            this.template = _.template($(options.template).html());
            this.onRegister = this.trigger.bind(this, 'onRegister');
        },

        /**
         * renders the view
         * @returns {UserRegisterView}
         */
        render: function () {
            $(this.el).html(this.template());
            $(this.el).attr('class', 'user-view user-register-view');
            return this;
        }
    });

    //User Login View
    UserLoginView = UserBaseView.extend({
        /**
         * template renderer
         * @type {Function}
         */
        template: null,

        events: {
            "click button.login": 'onLogin'
        },

        initialize: function (options) {
            this.template = _.template($(options.template).html());
            this.onLogin = this.trigger.bind(this, 'onLogin');
        },

        /**
         * renders the view
         * @returns {UserLoginView}
         */
        render: function () {
            $(this.el).html(this.template());
            $(this.el).attr('class', 'user-view user-login-view');
            return this;
        }
    });

    //User Recover View
    UserRecoverView = UserBaseView.extend({
        /**
         * template renderer
         * @type {Function}
         */
        template: null,

        events: {
            "click button.recover": 'onRecover'
        },

        initialize: function (options) {
            this.template = _.template($(options.template).html());
            this.onRecover = this.trigger.bind(this, 'onRecover');
        },

        /**
         * renders the view
         * @returns {UserRecoverView}
         */
        render: function () {
            $(this.el).html(this.template());
            $(this.el).attr('class', 'user-view user-recover-view');
            return this;
        }
    });

    ValidationErrorsView = Backbone.View.extend({
        tagName: 'div',

        render: function (validationErrors, fieldsLabels) {
            fieldsLabels = fieldsLabels || {};
            var $self = $(this.el).addClass('validationErrors');
            _.each(validationErrors, function (fieldErrors, field) {
                _.each(fieldErrors, function (errorMessage, validationKey) {
                    var fieldLabel = fieldsLabels[field] || field;
                    $self.append($('<div class="validationError"></div>')
                        .html("<b>" + t(fieldLabel) + "</b> " + t(errorMessage)));
                });
            });
            return this;
        }
    });

}());
