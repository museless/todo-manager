<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Config\Config;
use User\Model\UserTable;
use User\Model\User;
use User\Form\UserForm;

class UserController extends AbstractActionController
{
    const MSG_EROR_403 = 'User.loginErrors.InvalidCredentials';
    const MSG_EROR_412 = 'User.loginErrors.MissingCredentials';
    const MSG_ERROR_500 = 'User.loginErrors.InternalError';

    /**
     * @var User\Model\UserTable
     */
    private $userTable = null;
    
    /**
     * @var Zend\Config\Config
     */
    private $apiConfig = null;
    
    /**
     * Handle the request
     *
     * @param  MvcEvent $e
     * @return mixed
     * @throws Exception\DomainException if no route matches in event or invalid HTTP method
     */
    public function onDispatch(MvcEvent $e)
    {
        //catch any exception on the request, and give a generic JSON error message
        $ret = null;
        try {
            $ret = parent::onDispatch($e);
        } catch(\Exception $err) {
            $this->response->setStatusCode(500);/* Internal Server Error */
            $e->setResult(
                new JsonModel(array('data' => self::MSG_ERROR_500))
            );
        }
        return $ret;
    }
    
    public function indexAction()
    {
    }

    /**
     * 
     * @return User\Model\UserTable
     */
    public function getUserTable()
    {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }

    public function getApiConfig()
    {
        if (!$this->apiConfig) {
            $config = new Config($this->getServiceLocator()->get('Config'));
            $this->apiConfig = $config->todoApi;
        }
        return $this->apiConfig;
    }

    public function loginAction()
    {
        $return = null;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $email = $request->getPost('user_email', '');
            $password = $request->getPost('user_password', '');
            $config = $this->getApiConfig();
            $user = $this->getUserTable()->getUserByEmailAndPassword($email, md5($password));
            if ($user) {
                $return = $this->createApiToken($user->id, $config);
            } else {
                $this->response->setStatusCode(403);
                $return = array('data' => self::MSG_EROR_403);
            }
        } else {
            $this->response->setStatusCode(412);
            $return = array('data' => self::MSG_EROR_412);
        }
        return new JsonModel($return);
    }

    public function registerAction()
    {
        $return = null;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form = new UserForm();
            $user = new User();

            $postData = $this->getRequest()->getPost();
            $data = array();
            foreach ($postData as $key => $value) {
                $data[str_replace('user_', '', $key)] = $value;
            }
            
            $form->setInputFilter($user->getInputFilter()->remove('id'));
            $form->setData($data);

            if ($form->isValid()) {
                $user = $this->getUserTable()->getUserByEmail($data['email']);
                if (!$user) {
                    $user = new User();
                    $user->exchangeArray($form->getData());
                    $user->password = md5($user->password);
                    $id = $this->getUserTable()->saveUser($user);
                    if ($id) {
                        $config = $this->getApiConfig();
                        $return = $this->createApiToken($id, $config);
                    }
                } else {
                    $this->response->setStatusCode(412);
                    $return = array(
                        'data' => array(
                            'user_email'=>array('alreadyExists' => 'User.ValidationErrors.ChooseOtherEmail')
                        )
                    );
                }
            } else {
                $messages = $form->getMessages();
                $this->response->setStatusCode(412);
                $return = array('data' => $messages);
            }
        }
        return new JsonModel($return);
    }

    public function recoverAction()
    {
    }
}
