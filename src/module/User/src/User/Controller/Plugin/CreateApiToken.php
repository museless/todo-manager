<?php

namespace User\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class CreateApiToken extends AbstractPlugin
{
    /**
     * returns and api token
     *
     * @param string $user_id
     * @param Zend\Config\Config $password
     * @return mixed
     */
    public function __invoke($user_id, $config)
    {
        return $this->create($user_id, $config);
    }

    /**
     * returns and api token
     * @param string $user_id
     * @return array
     */
    public function create($user_id, $config)
    {
        $maxtime = time() + $config->timeout * 60;
        $digest = sha1($config->secret . $maxtime. $user_id);
        return array(
            'token' => strlen($maxtime) . $maxtime . $digest,
            'user_id' => $user_id
        );
    }
}