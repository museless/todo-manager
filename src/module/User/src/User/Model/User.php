<?php

namespace User\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class User implements InputFilterAwareInterface
{
    public $id;
    public $firstname;
    public $lastname;
    public $email;
    public $password;

    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id        = (isset($data['id'])) ? $data['id'] : null;
        $this->firstname = (isset($data['firstname'])) ? $data['firstname'] : null;
        $this->lastname  = (isset($data['lastname'])) ? $data['lastname'] : null;
        $this->email  = (isset($data['email'])) ? $data['email'] : null;
        $this->password  = (isset($data['password'])) ? $data['password'] : null;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'id',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'Int'),
                        ),
                        'validators' => array(
                            array(
                              'name' =>'NotEmpty', 
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'User.ValidationErrors.CantBeEmpty' 
                                    ),
                                ),
                            ),
                            array(
                                'name'    => 'Regex',
                                'options' => array(
                                    'pattern' => '/^\\d+$/',
                                    'message' => "must be a number",
                                ),
                            ),
                        ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'firstname',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                              'name' =>'NotEmpty', 
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'User.ValidationErrors.CantBeEmpty' 
                                    ),
                                ),
                            ),
                            array(
                                'name'    => 'StringLength',
                                'options' => array(
                                    'message' => 'User.ValidationErrors.0to100',
                                    'encoding' => 'UTF-8',
                                    'min'      => 1,
                                    'max'      => 100,
                                ),
                            ),
                        ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'lastname',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                              'name' =>'NotEmpty', 
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'User.ValidationErrors.CantBeEmpty' 
                                    ),
                                ),
                            ),
                            array(
                                'name'    => 'StringLength',
                                'options' => array(
                                    'message' => 'User.ValidationErrors.0to100',
                                    'encoding' => 'UTF-8',
                                    'min'      => 1,
                                    'max'      => 100,
                                ),
                            ),
                        ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'email',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                              'name' =>'NotEmpty', 
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'User.ValidationErrors.CantBeEmpty' 
                                    ),
                                ),
                            ),
                            array(
                                'name'    => 'EmailAddress',
                                'options' => array(
                                    'message' => 'User.ValidationErrors.MustBeEmail',
                                ),
                            ),
                        ),
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'password',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                              'name' =>'NotEmpty', 
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\NotEmpty::IS_EMPTY => 'User.ValidationErrors.CantBeEmpty' 
                                    ),
                                ),
                            ),
                            array(
                                'name'    => 'Regex',
                                'options' => array(
                                    'pattern' => '/^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/',
                                    'message' => "User.ValidationErrors.MixedPassword",
                                ),
                            ),
                        ),
                    )
                )
            );
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
