<?php
namespace User\Form;

use Zend\Form\Form;

class UserForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $this->add(
            array(
                'name' => 'id',
                'attributes' => array(
                    'type'  => 'hidden',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'firstname',
                'attributes' => array(
                    'type'  => 'text',
                ),
                'options' => array(
                    'label' => 'Firstname',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'lastname',
                'attributes' => array(
                    'type'  => 'text',
                ),
                'options' => array(
                    'label' => 'Lastname',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'email',
                'attributes' => array(
                    'type'  => 'text',
                ),
                'options' => array(
                    'label' => 'Email',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'password',
                'attributes' => array(
                    'type'  => 'text',
                ),
                'options' => array(
                    'label' => 'Password',
                ),
            )
        );
        $this->add(
            array(
                'name' => 'submit',
                'attributes' => array(
                    'type'  => 'submit',
                    'value' => 'Go',
                    'id' => 'submitbutton',
                ),
            )
        );
    }
}