<?php

namespace User\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\View\Renderer\RendererInterface;
use Zend\View\Helper\HelperInterface;

/**
 * adds the necessary scripts and return the login templates
 */
class User extends AbstractHelper
    implements HelperInterface
{
    /**
     * translations required for the user application
     * @var Array
     */
    private $_translationsTexts = array(
        'User.ValidationErrors.CantBeEmpty' => 'Value is required and can\'t be empty',
        'User.ValidationErrors.MustBeEmail' => 'Value must be an email address',
        'User.ValidationErrors.ChooseOtherEmail' => 'Choose another email, that one already exists',
        'User.ValidationErrors.0to100' => 'Value must be a value between of 100 character maximum',
        'User.ValidationErrors.MustBeAlphabetic' => 'Value have only alphabetic characters',
        'User.ValidationErrors.PasswordTooShort' => 'Value must have at least 8 characters',
        'User.ValidationErrors.MixedPassword' => 'Value must contain lowercase letters, upercase letters and numbers',
        'User.ValidationErrors.PasswordConfirmation' => 'Does not match the Password',
        'User.fieldLabels.Email' => 'Email',
        'User.fieldLabels.Firstname' => 'Firstname',
        'User.fieldLabels.Lastname' => 'Lastname',
        'User.fieldLabels.Password' => 'Password',
        'User.fieldLabels.PasswordConfirmation' => 'Password Confirmation',
        'User.formLabel.Login' => 'Login',
        'User.formLabel.Register' => 'Register',
        'User.formLabel.Recover' => 'Recover',
        'User.loginErrors.InvalidCredentials' => 'Your email or password is not valid, please try again',
        'User.loginErrors.InternalError' => 'There was an error during the processing, please try again.',
        'User.loginErrors.MissingCredentials' => 'Ops, something went wrong, you haven\'t sent all your information.',
        'User.loginMessages.LoginTitle' => 'Start using it!!.',
        'User.loginMessages.LoginDesc' => 'Begin to enjoy the application.',
        'User.loginMessages.Login' => 'Login',
        'User.loginMessages.Register' => 'Register',
        'User.loginMessages.RegisterDesc' => 'Discover the application',
        
        'User.registerMessages.Title' => 'Start using it!!.',
        'User.registerMessages.Description' => 'Begin to enjoy the application.',
        'User.registerMessages.Login' => 'Login',
        'User.registerMessages.Register' => 'Register',
        'User.registerMessages.LoginDesc' => 'Already have an account? then start using it right now!',

    );

    /**
     *
     * @var RendererInterface
     */
    private $_view;
    
    /**
     * adds the necessary scripts and return the login templates
     * @param  string $hashUrlTo url required (login, register, recover)
     * @return string login templates
     */
    public function __invoke($hashUrlTo = null)
    {
        if (isset($hashUrlTo)) {
            return $this->getHashUrlTo($hashUrlTo);
        }
        return $this;
    }

    public function getView()
    {
        return $this->_view;
    }
    
    public function setView(RendererInterface $view)
    {
        $this->_view = $view;
    }

    /**
     * returns the link hash for the requested item
     * @param string $hashUrlTo
     * @return string
     */
    public function getHashUrlTo($hashUrlTo)
    {
        return "#user/" . $hashUrlTo;
    }

    /**
     * return the translated texts
     * @return array
     */
    public function getTranslations()
    {
        $texts = array();

        foreach ($this->_translationsTexts as $key => $value) {
            $texts[$key] = $this->_view->translate($key);
            if ($texts[$key] === $key) {
                $texts[$key] = $value;
            }
        }

        return $texts;
    }

    /**
     * Adds the javascript and styles required for the application
     */
    public function includeHeadScripts()
    {
        $this->_view->headScript()
            ->appendFile($this->_view->basePath() . '/js/user/user.js');
        $this->_view->headLink()
            ->appendStylesheet($this->_view->basePath() . '/css/user/user.css');
    }

    /**
     * returns the html required for the application.
     * @param array $options
     */
    public function getApp($options)
    {
        return $this->_view->partial(
            'user/helper/app.phtml', 
            array(
                'options' => $options,
                'translations' => $this->getTranslations()
            )
        );
    }
}