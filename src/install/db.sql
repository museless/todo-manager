DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  id int(11) NOT NULL auto_increment,
  firstname varchar(100) NOT NULL,
  lastname  varchar(100) NOT NULL,
  email  varchar(255) NOT NULL,
  password  varchar(32) NOT NULL,
  PRIMARY KEY (id)
);
INSERT INTO `user` (firstname, lastname, email, password)
    VALUES  ('test', 'test', 'test@test.com', '098f6bcd4621d373cade4e832627b4f6');


DROP TABLE IF EXISTS `todo`;
CREATE TABLE `todo` (
  id int(11) NOT NULL auto_increment,
  user_id int(11) NOT NULL,
  description text NOT NULL,
  completed  tinyint(1) NOT NULL,
  duedate  timestamp NOT NULL,
  priority  int NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id)
  REFERENCES user(id)
  ON UPDATE CASCADE ON DELETE CASCADE,
);
INSERT INTO `todo` (description, completed, duedate, priority)
    VALUES  ('finish the toptal screening application', FALSE, "20131026235959", 100);
