(function () {
    "use strict";

    /*global window */
    /*global Backbone*/
    /*global $*/
    /*global _*/
    /*global t*/
    /*jslint unparam: true*/

    var global = window || {};

    global.RememberThisPage = Backbone.Router.extend({

        /**
         * css selector where the screens should load
         * @type {String}
         */
        renderTarget: null,

        /**
         * Content saved on first load
         * @type {String}
         */
        content: null,

        /**
         * 
         * @param {Object} options
         */
        initialize: function (options) {
            this.route("", "showThisPage", this.showThisPage.bind(this));

            if (options.aliases) {
                _.each(options.aliases, function (alias, idx) {
                    this.route(alias, "showThisPage" + idx, this.showThisPage.bind(this));
                }.bind(this));
            }

            this.renderTarget = options.renderTarget || "#content";
            this.content = $(this.renderTarget).html();
        },

        showThisPage: function () {
            $(this.renderTarget).html(this.content);
        }
    });
}());