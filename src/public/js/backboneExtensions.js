(function () {
    "use strict";

    /*global window*/
    /*global _*/
    /*global Backbone*/

    _.extend(Backbone.History.prototype, {
        fragmentHistory: [],
        maxFragmentHistory: 100,
        addFragmentToHistory: function () {
            this.fragmentHistory.unshift(Backbone.history.fragment);
            if (this.fragmentHistory.length > this.maxFragmentHistory) {
                this.fragmentHistory.pop();
            }
        },
        lookupFragmentHistory: function (backAmount) {
            backAmount = backAmount || 0;
            return this.fragmentHistory[backAmount] || null;
        },
        startFragmentHistory: function (maxAmount) {
            maxAmount = maxAmount || 100;
            this.on('route', this.addFragmentToHistory.bind(this));
        }
    });
    Backbone.history.startFragmentHistory();

    var prevRoute = Backbone.Router.prototype.route,
        currentApplication = {trigger: function () {}};

    _.extend(Backbone.Router.prototype, {
        onRouteApplication: function (callback) {
            if (currentApplication !== this) {
                var evtData = {proceed: true};
                currentApplication.trigger('onLeaveApplication', evtData);
                if (!evtData.proceed) {
                    this.navigateBack(0, {trigger: false, replace: true});
                    return;
                }
                this.trigger('onEnterApplication');
                currentApplication = this;
            }
            return callback.apply(this, [].constructor.prototype.slice.call(arguments, 1));
        },
        navigateBack: function (backAmount, options) {
            return this.navigate(
                Backbone.history.lookupFragmentHistory(backAmount),
                options
            );
        },
        route: function (route, name, callback) {
            if (!callback) callback = this[name];
            callback = this.onRouteApplication.bind(this, callback);
            prevRoute.apply(this, [route, name, callback]);
        }
    });
}());