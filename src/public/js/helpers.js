(function () {
    "use strict";

    /*global window*/
    /*global _*/

    /**
     * Translation helper t
     * receives string to translate
     * receives object to extend translations
     */
    var translations = {},
        replaceVars = function(t, vars) {
            _.each(vars, function (value, varname) {
                t = t.replace('{!' + varname + '}', value);
            });
            return t;
        },
        translate = function (text, vars) {
            if (translations[text] !== undefined) {
                if (vars) {
                    return replaceVars(translations[text], vars);
                }
                return translations[text];
            }
            return text;
        },
        extendTranslations = function (more) {
            translations = _.extend(translations, more);
            return window.t;
        };

    window.t = function (text, vars) {
        if (text instanceof Object) {
            return extendTranslations(text);
        }
        return translate(text, vars);
    };

}());