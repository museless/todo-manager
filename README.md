project-mauricio-prado
======================

Test Project Mauricio Prado

To install 
 -download a release from the RELEASES directory.
 - Decompress the tar.gz in any place
 - Copy the config/local.php.dist to config/local.php
 - Edit the config./local.php, fill the placeholders "YOUR USERNAME HERE", "YOUR PASSWORD HERE", "YOUR DB NAME", "DB HOST"
 - Download the file src/install/db.sql from the repository
 - Execute in your console mysql -uYOURUSER -pYOURPASSWORD YOURDBNAME < db.sql
 - Edit your hosts files to point "toptal-screening.localhost" to 127.0.0.1
 - Configure a new site in your apache:
   - Add a new file "todomanager" in the folder /etc/apache2/sites-enabled/
   - Fill that file with a this content:
     -  (Replace the directories with the directory where you decompressed the file)

```
    <VirtualHost *:80>
        ServerName toptal-screening.localhost
        DocumentRoot /var/vhosts/toptal-screening.localhost/public
        SetEnv APPLICATION_ENV "development"
        <Directory /var/vhosts/toptal-screening.localhost/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
```

